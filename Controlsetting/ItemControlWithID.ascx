﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemControlWithID.ascx.cs" Inherits="UserControl_ItemControlWithID" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }

    .auto-style2 {
        height: 40px;
    }

    .auto-style3 {
        height: 22px;
    }
</style>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || (charCode == 8) || (charCode == 110)) {
            return true;
        }
        else {
            return false;
        }
    }
    function MyFunc(strmsg) {
        switch (strmsg) {
            case 1: {
                alert("Deleted successfully!!");
            }
                break;
            case 2: {
                alert("Updated successfully!!");
            }
                break;
            case 3: {
                alert("Sorry enable to Update or delete the record please contact with admin!!");
            }
                break;
        }
    }
</script>
<script type="text/javascript">
    function ValidateFunction() {
       <%-- if (document.getElementById("<%=txt_id.ClientID%>").value == "") {
            alert('ID can not be blank');
            document.getElementById("<%=txt_id.ClientID%>").focus();
            return false;
        }--%>
        if (document.getElementById("<%=txt_title.ClientID%>").value == "") {
            alert('Title can not be blank');
            document.getElementById("<%=txt_title.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_subtitle.ClientID%>").value == "") {
            alert('Subtitle can not be blank');
            document.getElementById("<%=txt_subtitle.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_price.ClientID%>").value == "") {
            alert('Price can not be blank');
            document.getElementById("<%=txt_price.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_linkedurl.ClientID%>").value == "") {
            alert('LinkedURL time can not be blank');
            document.getElementById("<%=txt_linkedurl.ClientID%>").focus();
            return false;
        }

        var uploadcontrol = document.getElementById('<%=fu_images.ClientID%>').value;
        var reg = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG|.jpg|.JPG)$/;
        if (uploadcontrol.length > 0) {
            if (reg.test(uploadcontrol)) {
                return true;
            }
            else {
                alert('please select .JPEG,.GIF,.PNG files only!!');
                return false;
            }
        }
    }
</script>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode >= 48 && charCode <= 57 || charCode == 08) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<table class="auto-style1">
    <tr>
        <td class="auto-style2">Control ID :<asp:DropDownList ID="ddl_controlID" runat="server" Height="25px"></asp:DropDownList></td>
    </tr>
    <%-- <tr>
        <td>Item ID :<asp:TextBox ID="txt_id" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox></td>
    </tr>--%>
    <tr>
        <td>Item Title :<asp:TextBox ID="txt_title" runat="server" Style="margin-bottom: 0px"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Item Subtitle :<asp:TextBox ID="txt_subtitle" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="auto-style3">Price :<asp:TextBox ID="txt_price" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="auto-style3">Image URL :<asp:FileUpload ID="fu_images" runat="server" /></td>
    </tr>
    <tr>
        <td class="auto-style3">Linked URL :<asp:TextBox ID="txt_linkedurl" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" /></td>
    </tr>
</table>
<div>
    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="gv1" runat="server" AutoGenerateColumns="false"
        OnRowEditing="gv1_RowEditing"
        OnRowUpdating="gv1_RowUpdating"
        OnRowCancelingEdit="gv1_RowCancelingEdit"
        OnRowDeleting="gv1_RowDeleting" CssClass="gridview"
        OnPageIndexChanging="gv1_PageIndexChanging" PageSize="30" AllowPaging="true">
        <Columns>
            <asp:TemplateField HeaderText="Control Name">
                <ItemTemplate>
                    <asp:Label ID="lbl_ControlID" runat="server" Text='<%# Eval("ControlName")%>'></asp:Label>
                    <asp:Label ID="lbl_ItemID" runat="server" Visible="false" Text='<%# Eval("ItemID")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item Title">
                <ItemTemplate>
                    <asp:Label ID="lbl_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Item SubTitle">
                <ItemTemplate>
                    <asp:Label ID="lbl_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Price">
                <ItemTemplate>
                    <asp:Label ID="lbl_price" runat="server" Text='<%# Eval("Price")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_price" runat="server" onkeypress="return isNumberKey(this);" Text='<%# Eval("Price")%>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Linked URL">
                <ItemTemplate>
                    <asp:Label ID="lbl_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txt_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle CssClass="hdrow" />
                <HeaderTemplate>
                    <asp:Label ID="hlblimg" runat="server" Text="Image"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Image ID="img1" runat="server" ImageUrl='<%# Eval("ImageURL") %>'
                        Height="100px" Width="100px" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:FileUpload ID="fu1" runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle CssClass="hdrow" />
                <ItemTemplate>
                    <asp:Button ID="btnedit" runat="server" Text="Edit" CommandName="Edit" />
                    <asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Button ID="btnupdate" runat="server" Text="Update" CommandName="Update" />
                    <asp:Button ID="btnDelete" runat="server" Text="Calcel" CommandName="Cancel" />
                </EditItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
</div>
<%--<div class="large-12 medium-12 small-12">
    <asp:UpdatePanel ID="UP" runat="server">
        <ContentTemplate>
            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False"
                OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="30"
                CssClass="GridViewStyle" GridLines="None" ShowFooter="True" Width="100%">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:TemplateField HeaderText="Control ID">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ControlID" runat="server" Text='<%# Eval("ControlID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item ID">
                        <ItemTemplate>
                            <asp:Label ID="lbl_ItemID" runat="server" Text='<%# Eval("ItemID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item Title">
                        <ItemTemplate>
                            <asp:Label ID="lbl_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txt_title" runat="server" Text='<%# Eval("ItemTitle")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item SubTitle">
                        <ItemTemplate>
                            <asp:Label ID="lbl_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txt_subtitle" runat="server" Text='<%# Eval("ItemSubTitle")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price">
                        <ItemTemplate>
                            <asp:Label ID="lbl_price" runat="server" Text='<%# Eval("Price")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txt_price" runat="server" Text='<%# Eval("Price")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image">
                        <ItemTemplate>
                            <asp:Label ID="lbl_images" runat="server" Visible="false" Text='<%# Eval("ImageURL")%>'></asp:Label>
                            <asp:Image ID="img_url" runat="server" ImageUrl='<%# Eval("ImageURL") %>' Height="80px" Width="100px" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:FileUpload ID="imgUpload" runat="server" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Linked URL">
                        <ItemTemplate>
                            <asp:Label ID="lbl_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txt_linked" runat="server" Text='<%# Eval("LinkedURL")%>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowDeleteButton="True" />
                </Columns>
                <RowStyle CssClass="RowStyle" />
                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <SelectedRowStyle CssClass="SelectedRowStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <EditRowStyle CssClass="EditRowStyle" />
                <AlternatingRowStyle CssClass="AltRowStyle" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
        <ProgressTemplate>
            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
            </div>
            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                Please Wait....<br />
                <br />
                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                <br />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>--%>
