﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="col-group-wrapper row">
        <div class="submenu-item">
        <div class="row">
            <div class="col-md-6">
                 <ul class="submenu-item">
            <li class="nav-item"><a href="SprReports/ADMIN/QCTicketReport.aspx" class="nav-link">Booking Lookup</a></li>
            <li class="nav-item"><a href="SprReports/BulkMail.aspx" class="nav-link">Bulk Mail</a></li>
            <li class="nav-item"><a href="SprReports/ConfigureMails.aspx" class="nav-link">Configure Mails</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomHoldPNRRequest.aspx" class="nav-link">Dom. Hold PNR Request</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomHoldPNRUpdate.aspx" class="nav-link">Dom. Hold PNR Update</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomPendingPNRRequest.aspx" class="nav-link">Dom. Pending PNR Request  </a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomPendingPNRUpdate.aspx" class="nav-link">Dom. Pending Update</a></li>
            <li class="nav-item"><a href="SprReports/Refund/TktRptDom_RefundInProcess.aspx" class="nav-link">Dom. Refund In Process</a></li>
            <li class="nav-item"><a href="SprReports/Refund/TktRptDom_RefundRequest.aspx" class="nav-link">Dom. Refund Request</a></li>
                     </ul>
            </div>
            <div class="col-md-6">
                  <ul class="submenu-item">
            <li class="nav-item"><a href="SprReports/ADMIN/QCTicketReport.aspx" class="nav-link">Booking Lookup</a></li>
            <li class="nav-item"><a href="SprReports/BulkMail.aspx" class="nav-link">Bulk Mail</a></li>
            <li class="nav-item"><a href="SprReports/ConfigureMails.aspx" class="nav-link">Configure Mails</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomHoldPNRRequest.aspx" class="nav-link">Dom. Hold PNR Request</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomHoldPNRUpdate.aspx" class="nav-link">Dom. Hold PNR Update</a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomPendingPNRRequest.aspx" class="nav-link">Dom. Pending PNR Request  </a></li>
            <li class="nav-item"><a href="SprReports/HoldPNR/DomPendingPNRUpdate.aspx" class="nav-link">Dom. Pending Update</a></li>
            <li class="nav-item"><a href="SprReports/Refund/TktRptDom_RefundInProcess.aspx" class="nav-link">Dom. Refund In Process</a></li>
            <li class="nav-item"><a href="SprReports/Refund/TktRptDom_RefundRequest.aspx" class="nav-link">Dom. Refund Request</a></li>
                     </ul>
            </div>
        </div>
            </div>
       </div>
          
    </form>
</body>
</html>
