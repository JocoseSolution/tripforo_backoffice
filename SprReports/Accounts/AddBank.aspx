﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="AddBank.aspx.cs" Inherits="SprReports_Accounts_AddBank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">Add Bank Details</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox runat="server" name="bankname" placeholder="Bank Name" ID="bankname" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox runat="server" type="text" name="branchname" ID="branchname" placeholder="Branch Name" class="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox runat="server" type="text" name="area" ID="area" placeholder="Area" class="form-control"></asp:TextBox>


                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox runat="server" type="text" name="accno" ID="accno" placeholder="Account No." class="form-control"></asp:TextBox>


                        </div>
                    </div>

                    <div class="col-md-3" id="tr_BookingType" runat="server">
                        <div class="form-group">
                            <asp:TextBox runat="server" type="text" name="ifsc" ID="ifsc" placeholder="IFSC Code" class="form-control"></asp:TextBox>
                        </div>


                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <asp:DropDownList CssClass="form-control" ID="distributer" runat="server">
                                <asp:ListItem>--Select Distributer--</asp:ListItem>
                                <asp:ListItem>HEADOFFICE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="submit" runat="server" Text="Submit" OnClick="submit_Click" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <asp:Button ID="reset" OnClick="reset_Click" runat="server" Text="reset" CssClass="btn btn-danger" />
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <br />

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" CssClass="table table-hover"  AutoGenerateColumns="false" DataKeyNames="BankName"
                        OnRowDeleting="OnRowDeleting" OnRowDataBound="GridView1_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Bank" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" Visible="false" runat="server" Text='<%# Eval("Counter") %>'></asp:Label>
                                    <asp:Label ID="lblbankname" runat="server" Text='<%# Eval("BankName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtbankname" runat="server" Text='<%# Eval("BankName") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Branch" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranchname" runat="server" Text='<%# Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtbranchname" runat="server" Text='<%# Eval("BranchName") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Area" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblarea" runat="server" Text='<%# Eval("Area") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtarea" runat="server" Text='<%# Eval("Area") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Acc. No." ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblaccnum" runat="server" Text='<%# Eval("AccountNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtaccnum" runat="server" Text='<%# Eval("AccountNumber") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="IFSC" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblifsc" runat="server" Text='<%# Eval("NEFTCode") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtifsc" runat="server" Text='<%# Eval("NEFTCode") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>


                            <asp:CommandField ButtonType="Link" ShowDeleteButton="true" ItemStyle-Width="150" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </div>
</asp:Content>

