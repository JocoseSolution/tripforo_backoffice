﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SprReports_Accounts_AddBank : System.Web.UI.Page
{

      string cs = (ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindGrid();
        }

    }

    void clear()
    {
        bankname.Text = "";
        branchname.Text = "";
        area.Text = "";
        accno.Text = "";
        ifsc.Text = "";
        distributer.Text = "";
    }

    private void BindGrid()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Bank_Accounts"))
            {
                cmd.Parameters.AddWithValue("@Action", "SELECT");
                cmd.Parameters.AddWithValue("@BankName", "SELECT");
                cmd.Parameters.AddWithValue("@BranchName", "SELECT");
                cmd.Parameters.AddWithValue("@Area", "SELECT");
                cmd.Parameters.AddWithValue("@AccountNumber", "SELECT");
                cmd.Parameters.AddWithValue("@NEFTCode", "SELECT");
                cmd.Parameters.AddWithValue("@DISTRID", "SELECT");
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        GridView1.DataSource = dt;
                        GridView1.DataBind();
                    }
                }
            }
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton db = (LinkButton)e.Row.Cells[5].Controls[0];
            db.OnClientClick = "return confirm('Are you sure want to delete this bank detail?');";
        }
    }


    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridViewRow rowDeleting = (GridViewRow)GridView1.Rows[e.RowIndex];
        int thisid = Convert.ToInt32(((Label)rowDeleting.Cells[2].FindControl("lblId")).Text);

        if (thisid > 0)
        {
            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("Bank_Accounts"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Action", "DELETE");
                    cmd.Parameters.AddWithValue("@Id", thisid);

                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        this.BindGrid();
    }

    protected void submit_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(cs))
        {

            //SqlCommand cmd = new SqlCommand("sp_insert", con);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("name", bankname.Text);
            //cmd.Parameters.AddWithValue("email", branchname.Text);
            //cmd.Parameters.AddWithValue("education", area.Text);
            //cmd.Parameters.AddWithValue("phoneno", accno.Text);
            //cmd.Parameters.AddWithValue("city", ifsc.Text);
            //cmd.Parameters.AddWithValue("city", distributer.Text);
            con.Open();
            SqlCommand cmd = new SqlCommand("insert into BankDetails (BankName,	BranchName,	Area, AccountNumber, NEFTCode, DISTRID) values ('" + bankname.Text + "','" + branchname.Text + "', '" + area.Text + "','" + accno.Text + "', '" + ifsc.Text + "','" + distributer.Text + "')", con);
            int t = cmd.ExecuteNonQuery();
            if (t > 0)
            {
                Response.Write("<script>alert('Bank added successfully')</script>");
            }

            //int k = cmd.ExecuteNonQuery();
            //if (k != 0)
            //{
            //    lblmsg.Text = "Record Inserted Succesfully into the Database";
            //    lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            //}
            con.Close(); 

            clear();
        }
        this.BindGrid();
    }


    protected void reset_Click(object sender, EventArgs e)
    {
        bankname.Text = "";
        branchname.Text = "";
        area.Text = "";
        accno.Text = "";
        ifsc.Text = "";
        distributer.Text = "";
    }
}