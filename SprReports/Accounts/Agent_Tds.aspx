﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Agent_Tds.aspx.vb" Inherits="Reports_Accounts_Agent_Tds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--  <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
   <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>
    <style type="text/css">
        .txtBox {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }

        .txtCalander {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>
    <style>
        input[type="text"], input[type="password"], select {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>

    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">TDS</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 200px" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">

                            <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="btn btn-danger" />
                        </div>
                    </div>
                </div>
            </div>
            </div>
        <br />
            <br />
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">

                        <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" CssClass="GridViewStyle"
                            Width="100%">
                            <Columns>
                                <asp:CommandField ShowEditButton="True" />
                                <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />
                                <asp:BoundField DataField="Agency_Name" HeaderText="Agency Name" ControlStyle-CssClass="textboxflight1"
                                    ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="user_id" HeaderText="Agent ID" ControlStyle-CssClass="textboxflight1"
                                    ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="Agent_Type" HeaderText="Agent Type" ControlStyle-CssClass="textboxflight1"
                                    ReadOnly="True"></asp:BoundField>
                                <asp:BoundField DataField="TDS" HeaderText="Normal TDS %" ControlStyle-CssClass="textboxflight1"></asp:BoundField>
                                <%--<asp:BoundField DataField="ExmptTDS" HeaderText="Exempted TDS %" ControlStyle-CssClass="textboxflight1">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ExmptTdsLimit" HeaderText="Exempted TDS Limit" ControlStyle-CssClass="textboxflight1">
                                                        </asp:BoundField>--%>
                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ItemStyle-Font-Bold="true"
                                    Visible="false">
                                    <ItemStyle Font-Bold="True"></ItemStyle>
                                </asp:CommandField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>

                    </div>
        </div>
    </div>
    </div>
           
      
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
