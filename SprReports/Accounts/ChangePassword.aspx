﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" MasterPageFile="~/MasterAfterLogin.master" Inherits="SprReports_Accounts_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Profile</li>
                  <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txtOldPassword" runat="server" placeholder="Old Password" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter new password." ControlToValidate="TextBoxNewPass" ValidationGroup="cp"></asp:RequiredFieldValidator>
                                    <br />
                                   <asp:RegularExpressionValidator ID="Regex1" runat="server" ControlToValidate="TextBoxNewPass" ValidationGroup="cp"  Display="dynamic"
                                       ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,15}"
                                        ErrorMessage="Password must contain: Minimum 6 and Maximum 15 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />--%>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="TextBoxNewPass" runat="server" placeholder="New Password" TextMode="Password" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="TextBoxConfirmPass" placeholder="Confirm Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button runat="server" ID="ButtonSubmit" Text="Submit" CssClass="btn btn-danger" ValidationGroup="cp" OnClientClick="return validatePassword();" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">

                            <asp:Label ID="lblmsg" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </div>
                        </div>
                </div>


            </div>
        </div>
    </div>
    
    <script type="text/javascript">

        function validatePassword() {

            var o = $("#<%=txtOldPassword.ClientID%>").val();
            var x = $("#<%=TextBoxNewPass.ClientID%>").val();
            var y = $("#<%=TextBoxConfirmPass.ClientID%>").val();
            var errors = "";

            var oldPass = o.trim();
            var p = x.trim();
            var q = y.trim();
            if (oldPass == null || oldPass == "") {
                errors = "Please enter old password.\n";
                alert(errors);
                return false;
            }
            if (p == null || p == "") {

                errors = "Please enter New password.\n";
                alert(errors);
                return false;
            }

            if (q == null || q == "") {

                errors = "Please enter Confirm password.\n";
                alert(errors);
                return false;
            }
            debugger;
            if (p != q) {
                errors = "Password not Matched.\n";
                alert(errors);
                return false;

            }
            if (p.length < 6) {
                errors = "Your password must be at least 6 characters";
                alert(errors);
                return false;
            }
            if (p.search(/[a-z]/i) < 0) {
                errors = "Your password must contain at least one letter.";
                alert(errors);
                return false;
            }
            if (p.search(/[a-z]/) < 0) {
                errors = "Your password must contain at least one lowercase letter.";
                alert(errors);
                return false;
            }
            if (p.search(/[A-Z]/) < 0) {
                errors = "Your password must contain at least one uppercase letter.";
                alert(errors);
                return false;
            }
            if (p.search(/[0-9]/) < 0) {
                errors = "Your password must contain at least one digit.";
                alert(errors);
                return false;
            }
            return true;
        }
    </script>
</asp:Content>

