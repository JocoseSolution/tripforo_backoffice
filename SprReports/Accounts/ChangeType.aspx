﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ChangeType.aspx.cs" Inherits="SprReports_Accounts_ChangeType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">Change Group Type</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group" id="tr_AgentType" runat="server">
                            <label for="exampleInputPassword1" id="tr_GroupType" runat="server"></label>
                            <asp:DropDownList ID="DropDownListType" runat="server" CssClass="form-control">
                                <asp:ListItem Text="----Select Agent Type----" Value="Select" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" CssClass="btn btn-danger" />
                        </div>
                    </div>
                </div>
                <div class="alert_msg info_msg fl">
                    <b class="status_info fl">Note:- </b><span class="status_cont">Put All UserId In Excel Sheet1(Format .xls or .xlsx) In First Column And No Need To Create HeaderId</span>

                </div>

                <div id="msg" runat="server" style="width: 900px; display: none">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

