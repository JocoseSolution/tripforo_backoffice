﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Ledger.aspx.vb" Inherits="Reports_Accounts_Ledger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <link href="../../assets/css/demo_3/style.css" rel="stylesheet" />

    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">Ledger Details</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="From" placeholder="From Date" id="From" readonly="readonly" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="To" id="To" placeholder="To Date" readonly="readonly" class="form-control" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="tr_Agency" runat="server">
                            <span id="tr_AgencyName" runat="server">
                                <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                        </div>
                    </div>

                    <div class="col-md-3" id="tr_BookingType" runat="server">
                        <div class="form-group" id="lblBookType" runat="server" >
                            <asp:DropDownList ID="ddl_BookingType" placeholder="Booking Type" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>


                    </div>
                     <div class="col-md-3 ">
                        <div class="form-group">
                            <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>

                 <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <asp:Button ID="btnExpt" runat="server" Text="Export" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3" style="display: none;">
                        <div class="form-group" id="tr_Cat" runat="server">
                            <label for="exampleInputEmail1">Upload Category </label>
                            <asp:DropDownList ID="ddl_Category" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="tr_UploadType" runat="server">
                            <label for="exampleInputEmail1" id="lblUpType" runat="server">Upload Type</label>
                            <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                CssClass="form-control">
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="tr_SearchType" runat="server" visible="false">
                            <label for="exampleInputEmail1">Search Type</label>
                            <asp:RadioButton CssClass="form-control" ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                Text="Agent" />
                            <asp:RadioButton ID="RB_Distr" CssClass="form-control" runat="server" GroupName="Trip" onclick="Hide(this)"
                                Text="Own" />
                        </div>
                    </div>
                
                
                   


                   

                    

                    <div class="col-md-3 " style="display: none;">
                        <label for="exampleInputEmail1">PaymentMode :</label>
                        <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                            <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

    </div>
    </div>

    <br />
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Ledger Data</h4>

            <div class="col-12 ">
                <div class="dataTables_wrapper dt-bootstrap4 no-footer">


                    <div class="row ">
                        <div class="table-responsive">
                            <asp:UpdatePanel ID="up" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        class="table table-hover" role="grid" aria-describedby="order-listing_info" PageSize="30">

                                        <Columns>
                                            <asp:BoundField DataField="AgencyId" HeaderText="AgencyId" />
                                            <asp:BoundField DataField="AgentID" HeaderText="User_ID" />
                                            <asp:BoundField DataField="AgencyName" HeaderText="Agency_Name" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                            <asp:BoundField HeaderText="Sector" DataField="Sector"></asp:BoundField>
                                                <%-- <asp:BoundField HeaderText="Order No" DataField="InvoiceNo"></asp:BoundField>--%>                                                
                                                <asp:TemplateField HeaderText="Order ID">
                                                    <ItemTemplate>
                                                        <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("InvoiceNo")%> &TransID='
                                                            rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="OrderID" runat="server" Text='<%#Eval("InvoiceNo")%>'></asp:Label></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            <asp:BoundField HeaderText="Pax Name" DataField="PaxName"></asp:BoundField>
                                            <asp:BoundField HeaderText="Pnr" DataField="PnrNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="Aircode" DataField="Aircode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="TicketNo" DataField="TicketNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <%--<asp:BoundField HeaderText="Easy ID" DataField="YatraAccountID" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                            <asp:BoundField HeaderText="DR." DataField="Debit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="CR." DataField="Credit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="Balance" DataField="Aval_Balance"></asp:BoundField>
                                            <asp:BoundField HeaderText="Booking Type" DataField="BookingType" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="Remark" DataField="Remark" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>

                                            <asp:BoundField HeaderText="DueAmount" DataField="DueAmount" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                            <asp:BoundField HeaderText="CreditLimit" DataField="CreditLimit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>

                                            <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                        </Columns>

                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    </div>




    <script type="text/javascript">
    var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
