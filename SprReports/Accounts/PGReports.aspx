﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="PGReports.aspx.vb" Inherits="SprReports_Accounts_PGReports" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />




                <div class="content-wrapper">
                

                                 <div class="page-header">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">PGReport </li>
                </ol>
              </nav>
       </div>
                    <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" id="To" placeholder="To Date" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server" placeholder="Order Id" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputEmail1">Status :</label>--%>
                                    <asp:DropDownList CssClass="form-control" ID="drpPaymentStatus" runat="server">
                                        <asp:ListItem>---Select Status---</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-3" id="td_Agency" runat="server">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Agency Name</label>--%>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" placeholder="Agency Name" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                </div>
                            </div>
                            <div class="col-md-3">
                               <%-- <label for="exampleInputEmail1">PaymentMode :</label>--%>
                                <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="--select Payment Type ---" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="CreditCard" Value="CC"></asp:ListItem>
                                    <asp:ListItem Text="DebitCard" Value="DC"></asp:ListItem>
                                    <asp:ListItem Text="NetBanking" Value="NB"></asp:ListItem>
                                    <%-- <asp:ListItem Text="CreditCard" Value="credit card"></asp:ListItem>
                                   <asp:ListItem Text="DebitCard" Value="debit card"></asp:ListItem>
                                  <asp:ListItem Text="NetBanking" Value="net banking"></asp:ListItem>   
                                  <asp:ListItem Text="Cash Card" Value="cash card"></asp:ListItem>   
                                  <asp:ListItem Text="Mobile Payment" Value="mobile payment"></asp:ListItem>   --%>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                   
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-danger" />

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-danger" />
                                </div>
                            </div>
                        </div>

                       

                            <div class="alert_msg info_msg fl"><b class="status_info fl">Note: </b><span class="status_cont">To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.</span></div>

                       
                        
                        </div>
                    </div>
                    <br />
                    <br />
                     <div class="card">
                    <div class="card-body">

                        <div class="table-responsive">
                            <div id="divReport" runat="server" visible="true">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="grd_IntsaleRegis" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                        CssClass="table" GridLines="None" PageSize="30" OnRowCommand="OnRowCommand">
                                                        <Columns>

                                                            <%--  <asp:TemplateField HeaderText="Order Id">
                                                                <ItemTemplate>
                                                                    <a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("OrderId")%>&amp;invno=<%#Eval("OrderId")%>&amp;tktno=<%#Eval("OrderId")%>&amp;AgentID=<%#Eval("AgentId")%>'
                                                                        style="color: #004b91; font-size: 11px; font-weight: bold" target="_blank">
                                                                        <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                                       (Check Response )</a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>  --%>

                                                            <asp:TemplateField HeaderText="Check Status">
                                                                <ItemTemplate> 
                                                                    <a href='CheckPGResponse.aspx?OrderId=<%#Eval("OrderId")%>&amp;AgentID=<%#Eval("AgentId")%>'
                                                                        rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <%--<asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>                                                                       
                                                                        Check Status </a>                                                                  
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                             <asp:TemplateField HeaderText="Order Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             
                                                             <asp:TemplateField HeaderText="UserId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="AgencyId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="AGENCY&nbsp;NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                              <asp:TemplateField HeaderText="UnmappedStatus">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="UnmappedStatus" runat="server" Text='<%#Eval("UnmappedStatus")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Api_Status">                                                                
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ApiStatus" runat="server" Text='<%#Eval("ApiStatus")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tracking Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Trackingid" runat="server" Text='<%#Eval("Trackingid")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Bank RefNo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="BankRefNo" runat="server" Text='<%#Eval("BankRefNo")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="TId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TId" runat="server" Text='<%#Eval("TId")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderText="Original Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OriginalAmount" runat="server" Text='<%#Eval("OriginalAmount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Convenience Fee">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TotalCharges" runat="server" Text='<%#Eval("TotalCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                          
                                                            <asp:TemplateField HeaderText="Dis. Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="DiscountValue" runat="server" Text='<%#Eval("DiscountValue")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Merchant Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MerAamount" runat="server" Text='<%#Eval("MerAamount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                             <asp:TemplateField HeaderText="TotalAmount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TotalAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                           
                                                            <asp:TemplateField HeaderText="Wallet-Update">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CreditLimitUpdate" runat="server" Text='<%#Eval("CreditLimitUpdate")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Error Message">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ErrorText" runat="server" Text='<%#Eval("ErrorText")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Service Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ServiceType" runat="server" Text='<%#Eval("ServiceType")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Payment Mode">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                            <asp:TemplateField HeaderText="Card/Bank Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CardName" runat="server" Text='<%#Eval("CardName")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                           
                                                            <asp:TemplateField HeaderText="Created Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <%--<asp:ButtonField CommandName="ButtonField" Text="Check-Status" ButtonType="Button" />--%>
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="DivPrint" runat="server" visible="true"></div>
                        </div>
                         </div>
                    
                </div>
    

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>





