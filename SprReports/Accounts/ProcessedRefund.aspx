﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ProcessedRefund.aspx.cs" Inherits="SprReports_Accounts_ProcessedRefund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }
    </style>
    <div class="content-wrapper">

        <div class="card">

            <div class="card-body">


                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">PG Refund Requested Report</li>
                       
                 </ol>
              </nav>



                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_OrderId" class="form-control" placeholder="Order Id" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3" style="display: none">
                        <label for="exampleInputPassword1">PNR No</label>
                        <asp:TextBox ID="txt_Pnrno" class="form-control" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-danger" OnClick="btn_result_Click" />
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Label ID="lbl_Norecord" runat="server"></asp:Label>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Total debit amount :</label>
                            <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Total refunded amount  :</label>
                            <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                        </div>

                    </div>

                    <div class="col-md-4" id="adb" runat="server" visible="false">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Max Refundable Convenience Fees :</label>
                            <asp:Label ID="lblmaxpg" runat="server"></asp:Label>
                        </div>
                    </div>


                    <div class="col-md-3">

                        <label for="exampleInputPassword1">Max amount can be refund :</label>
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </div>

                    <div class="col-md-3">

                        <asp:LinkButton ID="lnkupdate" runat="server" Text='Click Here To Refund' ForeColor="Green" Visible="false"
                            Font-Bold="true" Font-Size="11px" CommandName="lnkupdate"
                            OnClick="lnkupdate_Click"></asp:LinkButton>

                    </div>

                    <div class="col-md-8">

                        <asp:Label ID="lbltxt1" runat="server" ForeColor="Red"></asp:Label>
                    </div>

                </div>
            </div>
    </div>
  

   <br />
    <br />
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridRefunddtl" runat="server" AutoGenerateColumns="False" CssClass="table" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="OrderId">
                            <ItemTemplate>
                                <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AgentId">
                            <ItemTemplate>
                                <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PNR">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Pnr" runat="server" Text='<%#Eval("Pnr") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Agencyname">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Agencyname" runat="server" Text='<%#Eval("Agencyname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RefundedBy">
                            <ItemTemplate>
                                <asp:Label ID="lbl_RefundedBy" runat="server" Text='<%#Eval("RefundedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RefundedDate">
                            <ItemTemplate>
                                <asp:Label ID="lbl_RefundedDate" runat="server" Text='<%#Eval("RefundedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remark">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Remark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:TemplateField HeaderText="Pnr">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Pnr" runat="server" Text='<%#Eval("Pnr")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Convenience Fee">
                            <ItemTemplate>
                                <asp:Label ID="lbl_PgCharge" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Refund Amount">
                            <ItemTemplate>
                                <asp:Label ID="lbl_RefundAmount" runat="server" Text='<%#Eval("RefundAmount")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TDS">
                            <ItemTemplate>
                                <asp:Label ID="lbl_TDS" runat="server" Text='<%#Eval("TDS")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Mode">
                            <ItemTemplate>
                                <asp:Label ID="lbl_Paymentmode" runat="server" Text='<%#Eval("PgMode")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                </asp:GridView>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_btn_result').click(function (event) {
                if ($.trim($("#ctl00_ContentPlaceHolder1_txt_OrderId").val()) == "") {
                    if ($.trim($("#ctl00_ContentPlaceHolder1_txt_Pnrno").val()) == "") {
                        alert("Please enter OrderId Or PNR ");
                        $("#<%=txt_OrderId.ClientID%>").focus();
                        return false;
                    }
                }
            });


        });

    </script>


</asp:Content>

