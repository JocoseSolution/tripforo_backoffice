﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="Agent_Details.aspx.vb" Inherits="Reports_Admin_Agent_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Profile</li>
                  <li class="breadcrumb-item active" aria-current="page"> Agency Details</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="From" id="From" placeholder="Registration From" class="form-control" readonly="readonly" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="To" id="To" placeholder="Registration To" class="form-control" readonly="readonly" />
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"
                                class="form-control" />
                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="td_SBS" runat="server">
                            <label for="exampleInputPassword1" id="td_ddlSBS" runat="server"></label>
                            <asp:DropDownList ID="ddl_stock" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Select Search By Stockist" Value=""></asp:ListItem>
                                <asp:ListItem Text="All Stockist" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="Stockist Agent" Value="STAG"></asp:ListItem>
                                <asp:ListItem Text="Tripforo Agent" Value="HEADOFFICE"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="tr_AgentType" runat="server">
                            <label for="exampleInputPassword1" id="tr_GroupType" runat="server"></label>
                            <asp:DropDownList ID="DropDownListType" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Select Agent Type" Value="Select" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_Search" runat="server" Text="Search" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="export" runat="server" Text="Export" CssClass="btn btn-danger" />

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="tr_SalesPerson" runat="server">
                            <label for="exampleInputPassword1" id="tr_ddlSalesPerson" runat="server">Search Sales Person :</label>
                            <asp:DropDownList ID="DropDownListSalesPerson" runat="server" AppendDataBoundItems="true"
                                CssClass="form-control">
                                <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">



                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                AllowSorting="True" PageSize="25" CssClass="table" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Debit/Credit" SortExpression="user_id" Visible="false">
                                        <ItemTemplate>
                                            <a target="_blank" href="../Distr/UploadAmount.aspx?AgentID=<%#Eval("user_id")%>"
                                                rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Debit/Credit </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agency Name" SortExpression="Agency_Name" ItemStyle-ForeColor="Black">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User_Id" SortExpression="user_id" ItemStyle-ForeColor="Black">
                                        <ItemTemplate>
                                            <a href='Update_Agent.aspx?AgentID=<%#Eval("user_id")%>' rel="lyteframe" rev="width: 1000px; height: 700px; overflow:hidden;"
                                                target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("user_id")%>'></asp:Label>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AgencyId" HeaderText="Agency ID" SortExpression="AgencyId" />
                                    <asp:BoundField DataField="Agent_Type" HeaderText="Agent_Type" SortExpression="Agent_Type" />
                                    <%-- <asp:BoundField DataField="Crd_Limit" HeaderText="Credit Limit" SortExpression="Crd_Limit" />--%>
                                    <asp:BoundField DataField="timestamp_create" HeaderText="Registration_Date" SortExpression="timestamp_create" />
                                    <asp:BoundField DataField="Crd_Trns_Date" HeaderText="Transaction_Date" SortExpression="timestamp_create" />
                                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="SalesExecID" HeaderText="Sales Ref." SortExpression="SalesExecID" />
                                    <asp:BoundField DataField="Agent_status" HeaderText="Status" SortExpression="Agent_status" />
                                    <asp:BoundField DataField="Online_Tkt" HeaderText="Online_Tkt" SortExpression="Online_Tkt" />
                                    <%--<asp:BoundField DataField="Agent_Payout_Status" HeaderText="DMT Payout" SortExpression="Agent_Payout_Status" />--%>
                                    <asp:BoundField DataField="Distr" HeaderText="Dist" SortExpression="Balance" />
                                    <asp:BoundField DataField="Balance" HeaderText="Balance" SortExpression="Balance" />
                                    <asp:BoundField DataField="DueAmount" HeaderText="DueAmount" SortExpression="DueAmount" />
                                    <asp:BoundField DataField="AgentLimit" HeaderText="AgentLimit" SortExpression="AgentLimit" />
                                    <asp:BoundField DataField="NamePanCard" HeaderText="NamePanCard" SortExpression="NamePanCard" />
                                    <asp:BoundField DataField="PanNo" HeaderText="PanNo" SortExpression="AgentLimit" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" Font-Bold="False" Font-Strikeout="False" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
        </div>
    </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


</asp:Content>
