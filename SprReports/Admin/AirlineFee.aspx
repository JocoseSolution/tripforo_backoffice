﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="AirlineFee.aspx.vb" Inherits="Reports_Admin_AirlineFee" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../../chosen/chosen.css" />

    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>

    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode


            if (!(charCode == 46 || charCode == 48 || charCode == 49 || charCode == 50 || charCode == 51 || charCode == 52 || charCode == 53 || charCode == 54 || charCode == 55 || charCode == 56 || charCode == 57 || charCode == 8)) {
                //                alert("This field accepts only Float Number.");
                return false;


            }

            status = "";
            return true;

        }


    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting </li>
                  <li class="breadcrumb-item active" aria-current="page">Airline Fee</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Service Tax</label>
                            <asp:TextBox runat="server" ID="txt_SeviceTax"  CssClass="form-control"
                                onKeyPress="return checkit(event)" Text="Service Tax"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_SeviceTax"
                                ErrorMessage="*" Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Transaction Fee</label>
                            <asp:TextBox runat="server" ID="txt_TrasFee" placeholder="Transaction Fee" CssClass="form-control"
                                onKeyPress="return checkit(event)" Text="0"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_TrasFee"
                                ErrorMessage="*" Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">IATA Commission</label>
                            <asp:TextBox runat="server" ID="txtIataComm" placeholder="IATA Commission" CssClass="form-control"
                                onKeyPress="return checkit(event)" Text="0"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RQFV" runat="server" ControlToValidate="txtIataComm"
                                ErrorMessage="*" Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Airline Code</label>
                            <asp:DropDownList CssClass="form-control" runat="server" ID="airline_code">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Domestic" Value="D" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="International" Value="I"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-danger" Text="New Entry" OnClientClick="return confirm('Are you sure you want to add this?');" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">

                            <asp:Button ID="btn_Submit" runat="server" Text="Search" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Label ID="lbl" runat="server" Font-Bold="True" Style="color: #CC0000;"></asp:Label>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="UP" runat="server">
                        <ContentTemplate>

                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                CssClass="table" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" />
                                    <%-- <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />--%>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ID" runat="server" Text='<%#Eval("Counter") %>' CssClass="hide"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:BoundField DataField="UserId" HeaderText="Agent ID" ControlStyle-CssClass="textboxflight1"
                                            ReadOnly="true" />--%>
                                    <asp:TemplateField HeaderText="AirlineCode">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAirlineCode" runat="server" Text='<%# Eval("AirlineCode")%>'></asp:Label>

                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblAirlineCodeE" runat="server" Text='<%# Eval("AirlineCode")%>'></asp:Label>
                                            <asp:Label ID="lblSrNo" Visible="false" runat="server" Text='<%# Eval("Counter")%>' CssClass="hide"></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateField>



                                    <%--<asp:BoundField DataField="AirlineCode" HeaderText="AirlineCode" ControlStyle-CssClass="textboxflight1"
                                                    ReadOnly="true">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>--%>

                                    <asp:TemplateField HeaderText="Service Tax">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrvTax" runat="server" Text='<%# Eval("SrvTax")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtSrvTax" runat="server" Text='<%# Eval("SrvTax")%>'></asp:TextBox>

                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TransFee">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTranFee" runat="server" Text='<%# Eval("TranFee")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTranFee" runat="server" Text='<%# Eval("TranFee")%>'></asp:TextBox>

                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="SrvTax" HeaderText="Service Tax" ControlStyle-CssClass="textboxflight1">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TranFee" HeaderText="TransFee" ControlStyle-CssClass="textboxflight1">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IATAComm" HeaderText="IATA Comm." ControlStyle-CssClass="textboxflight1">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="IATA Commission.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIataComm" runat="server" Text='<%# Eval("IATAComm")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtIataComm" runat="server" Text='<%# Eval("IATAComm")%>'></asp:TextBox>

                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Trip" HeaderText="Trip" ControlStyle-CssClass="textboxflight1"
                                        ReadOnly="true">
                                        <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                    </asp:BoundField>
                                    <asp:CommandField ShowDeleteButton="True" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
