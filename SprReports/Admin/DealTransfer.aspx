﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="DealTransfer.aspx.cs" Inherits="SprReports_Admin_DealTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <script type="text/javascript">
        function Validate() {
            var ddl_ptype = document.getElementById("<%=ddl_ptype.ClientID %>");
            var ddl_ptype_To = document.getElementById("<%=ddl_ptype_To.ClientID %>");

            if (ddl_ptype.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select GroupType From!");
                return false;
            }

            if (ddl_ptype_To.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select GroupType To!");
                return false;
            }
        }
    </script>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">Deal Transfer</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" TabIndex="2">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="ddl_ptype_To" CssClass="form-control" runat="server" TabIndex="2">
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-danger" OnClick="Submit_Click" OnClientClick="return Validate()" />
                        </div>
                    </div>


                </div>
                <div class="alert_msg info_msg fl" id="DD">
                    <b class="status_info fl">Note: </b><span class="status_cont">Your DealTransferFrom as you Select which will be insert into DealTransferTo and Previous Deal of DealTransferTo Will be Deleted</span>

                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

