﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="GalTKTSwitch.aspx.cs" Inherits="SprReports_Admin_GalTKTSwitch" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">Gal Ticketing Switch</li>
                </ol>
              </nav>

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="ddl_gds_tktsearch" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddl_gds_tktsearch_SelectedIndexChanged">
                                <asp:ListItem Selected="False" Value="0">---Select--</asp:ListItem>

                                <asp:ListItem Selected="True" Value="D">Domestic</asp:ListItem>
                                <asp:ListItem Value="I">International</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Label ID="lbl_status" runat="server" Font-Bold="True" ForeColor="#009933" Width="550px"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:UpdatePanel runat="server" ID="UP_GDS_TKT_Search">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GV_GDS_TKT_Search" runat="server" AllowPaging="True" AutoGenerateColumns="false" CssClass="table" GridLines="None"
                                DataKeyNames="Counter" OnPageIndexChanging="GV_GDS_TKT_Search_PageIndexChanging" Width="100%">

                                <Columns>
                                    <asp:TemplateField HeaderText="HAP ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_HAP" runat="server" Text='<%#Eval("HAP") %>'></asp:Label>
                                            <asp:Label ID="lbl_counter" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_UserId" runat="server" Text='<%#Eval("UserId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PCC">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PCC" runat="server" Text='<%#Eval("PCC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Airline">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Airline" runat="server" Text='<%# Eval("Airline")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText=" Online Ticketing Status">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_Status" runat="server" AutoPostBack="true" OnCheckedChanged="chk_Status_CheckedChanged" Checked='<%# bool.Parse(Eval("TicketingStatus").ToString()) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Trip Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_TT" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                            </div>

                                          <div class="col-md-6">
                                              <div class="form-group"></div>
                                          </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UP_GDS_TKT_Search">
                        <ProgressTemplate>
                            <div class="modal">
                                <div class="center">
                                    Please Wait....
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

