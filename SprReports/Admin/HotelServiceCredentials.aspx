﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="HotelServiceCredentials.aspx.cs" Inherits="SprReports_Admin_HotelServiceCredentials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="../../newcss/sb-admin.css" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 10px;
            
        }
    </style>
    <div class="row">
        <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Hotel Setting > Search Hotel Supplier</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                                                    
 
                            <div class="col-md-4" id="divCorporateID">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Company ID</label>
                                    <asp:TextBox ID="TxtCorporateID" CssClass="form-control" runat="server" MaxLength="200"  TabIndex="1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">User ID</label>
                                    <asp:TextBox ID="TxtUserID" CssClass="form-control" runat="server" MaxLength="200"  TabIndex="2"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" MaxLength="200"  TabIndex="3"></asp:TextBox>
                                </div>
                            </div>
                            </div>

                        <div class="row"> 
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip Type</label>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="4">
                                         <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                        <asp:ListItem Value="Domestic" Text="Domestic" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="International" Text="International"></asp:ListItem>
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Supplier</label>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="form-control" TabIndex="5">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="GAL" Text="GAL"></asp:ListItem>    
                                        <asp:ListItem Value="ZUMATA" Text="ZUMATA"></asp:ListItem>
                                        <asp:ListItem Value="ZUMATABooking" Text="ZUMATABooking"></asp:ListItem>
                                         <asp:ListItem Value="TG" Text="TG"></asp:ListItem>
                                        <asp:ListItem Value="TGBooking" Text="TGBooking"></asp:ListItem>     
                                         <asp:ListItem Value="INNSTANT" Text="INNSTANT"></asp:ListItem>
                                         <asp:ListItem Value="GRN" Text="GRN"></asp:ListItem>
                                        <asp:ListItem Value="ROOMXML" Text="ROOMXML"></asp:ListItem>                                                                                
                                    </asp:DropDownList>
                                </div>
                            </div>   
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control"  TabIndex="6">
                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>      
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                   <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return Check();"  TabIndex="7" />
                                </div>
                            </div>                    
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12" style="overflow:auto;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow:auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="grd_HotelSwitchCredencials" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="20" OnRowDeleting="OnRowDeleting" OnRowDataBound="OnRowDataBound" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" OnRowCancelingEdit="grd_HotelSwitchCredencials_RowCancelingEdit" OnRowEditing="grd_HotelSwitchCredencials_RowEditing" OnRowUpdating="grd_HotelSwitchCredencials_RowUpdating">
                                            <Columns>
                                                 <asp:BoundField DataField="HotelTrip" HeaderText="TripType" ReadOnly="true" />
                                                 <asp:BoundField DataField="HotelCompanyID" HeaderText="Company ID" ReadOnly="true" />
                                                 <asp:BoundField DataField="HotelUsername" HeaderText="UserID" ReadOnly="true" />
                                                 <asp:BoundField DataField="HotelPassword" HeaderText="Password" ReadOnly="true" />
                                                 <asp:BoundField DataField="HotelProvider" HeaderText="Supplier" ReadOnly="true" />
                                                 
                                                 <asp:TemplateField HeaderText="Status">
                                                    <EditItemTemplate >
                                                        <asp:DropDownList ID="grdDdlStatus" runat="server" CssClass="form-control"  TabIndex="6" >
                                                        <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                     <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                             
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit" Text="EDIT"
                                                ImageUrl="~/Images/edit.png" ToolTip="Edit" Height="16px" Width="20px" ></asp:ImageButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:ImageButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="UPDATE" ValidationGroup="group1"
                                                ImageUrl="~/Images/ok.png" ToolTip="Update" Height="16px" Width="20px"></asp:ImageButton>
                                            <asp:ImageButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="CANCEL" ToolTip="Cancel" Height="16px" Width="20px"
                                                ForeColor="#20313f" Font-Strikeout="False" Font-Overline="False" Font-Bold="true" ImageUrl="../../Images/cancel1.png"></asp:ImageButton>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btn_delete" runat="server" Text="Delete" CommandName="Delete"  CausesValidation="True" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true"  ToolTip="Delete" Height="16px" Width="20px"
                                                ImageUrl="~/Images/delete.png" ></asp:ImageButton>
                                                    </ItemTemplate>
                                                  
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        </div>
    
     <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
   <%-- <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>--%>

    <script type="text/javascript">
     
        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select Supplier.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
                alert("Enter Agency Code.");
                $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
                alert("Enter User ID.");
                $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
                alert("Enter Password.");
                $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
                return false;
            }        

        }

    </script>

</asp:Content>

