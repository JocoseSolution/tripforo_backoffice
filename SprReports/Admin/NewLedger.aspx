﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="NewLedger.aspx.vb" Inherits="Reports_Accounts_NewLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }
    </style>


    <div class="content-wrapper">

        <div class="card">

            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">Ledger Single Orderid</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <input type="text" name="From" id="From" placeholder="From Date" readonly="readonly" class="form-control" />

                    </div>
                    <div class="col-md-3">
                        <input type="text" name="To" id="To" placeholder="To Date" readonly="readonly" class="form-control" />
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="tr_Agency" runat="server">
                            <span id="tr_AgencyName" runat="server">
                                <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" class="form-control" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                        </div>
                    </div>




                   
                     <div class="col-md-3">
                        <div class="form-group" id="tr_BookingType" runat="server">

                            <asp:DropDownList ID="ddl_BookingType" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="card-body" style="display: none">
                        <label for="exampleInputEmail1" id="lblBookType" runat="server">Booking Type</label>

                        <div class="col-md-3 " style="display: none;">
                            <label for="exampleInputEmail1">PaymentMode :</label>
                            <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="btn btn-danger" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-danger" />
                        </div>
                    </div>


                     <div class="col-md-3" style="display: none;">
                        <div class="form-group" id="tr_Cat" runat="server">
                            <label for="exampleInputEmail1">Upload Category </label>
                            <asp:DropDownList ID="ddl_Category" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="tr_UploadType" runat="server">
                            <label for="exampleInputEmail1" id="lblUpType" runat="server">Upload Type</label>
                            <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                CssClass="form-control">
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="tr_SearchType" runat="server" visible="false">
                            <label for="exampleInputEmail1">Search Type</label>
                            <asp:RadioButton CssClass="form-control" ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                Text="Agent" />
                            <asp:RadioButton ID="RB_Distr" CssClass="form-control" runat="server" GroupName="Trip" onclick="Hide(this)"
                                Text="Own" />
                        </div>
                    </div>

                   
                   
                    
                </div>
            </div>
        </div>




        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">

                    <asp:UpdatePanel ID="up" runat="server">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CssClass="table" PageSize="30">

                                <Columns>
                                    <asp:BoundField DataField="AgencyID" HeaderText="AgencyID" />
                                    <asp:BoundField HeaderText="Created Date" DataField="CreatedDate1" DataFormatString="{0:dd/MMM/yyyy}"></asp:BoundField>

                                    <asp:TemplateField HeaderText="OrderNo/Invoice">
                                        <ItemTemplate>

                                            <span><%#Eval("Link")%>
                                                <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("InvoiceNo")%>'></asp:Label><br />
                                                </a>
                                            </span>



                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Pnr">
                                        <ItemTemplate>
                                            <asp:Label ID="Pnr" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label>


                                            <%-- <a href='<%#Eval("InvoiceLink")%>' rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                <asp:Label ID="Pnr" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label></a>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>






                                    <asp:BoundField HeaderText="Aircode" DataField="TicketingCarrier"></asp:BoundField>
                                    <%--<asp:BoundField HeaderText="TicketNo" DataField="TicketNo"  ></asp:BoundField>  --%>
                                    <asp:BoundField HeaderText="DR." DataField="Debit"></asp:BoundField>
                                    <asp:BoundField HeaderText="CR." DataField="Credit"></asp:BoundField>
                                    <asp:BoundField HeaderText="Balance" DataField="Aval_Balance"></asp:BoundField>
                                    <%--<asp:BoundField HeaderText="DueAmount" DataField="DueAmount"></asp:BoundField>                                    --%>
                                    <asp:BoundField HeaderText="Booking Type" DataField="BookingType"></asp:BoundField>

                                    <asp:BoundField HeaderText="Narration" DataField="Narration"></asp:BoundField>
                                    <asp:BoundField HeaderText="Remark" DataField="Remark"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>







    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
