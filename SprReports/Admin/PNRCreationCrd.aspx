﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="PNRCreationCrd.aspx.cs" Inherits="SprReports_Admin_PNRCreationCrd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 35px;
            width: 100%;
        }

        .buttonBlue {
            height: 26px;
        }
    </style>
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">PNR Creation Credential</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                <asp:ListItem Value="" Text="-----Select Trip Type-----" Selected="False"></asp:ListItem>
                                <asp:ListItem Value="D" Text="Domestic" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="I" Text="International"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="DdlProvider" runat="server" CssClass="form-control" TabIndex="2">
                                <asp:ListItem Value="0" Text="--Select Provider--" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>

                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" tabindex="3" />
                            <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                        </div>
                    </div>

                    <div class="col-md-3" id="divCorporateID">
                        <div class="form-group">
                            <asp:TextBox ID="TxtCorporateID" placeholder="Corporate ID" CssClass="form-control" runat="server" MaxLength="50" TabIndex="4"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="TxtUserID" placeholder="User ID" CssClass="form-control" runat="server" MaxLength="50" TabIndex="5"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="TxtPassword" placeholder="Password" runat="server" CssClass="form-control" MaxLength="50" TabIndex="6" OnTextChanged="TxtPassword_TextChanged"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3" id="divCarrierPCC">
                        <div class="form-group">
                            <asp:TextBox ID="TxtCarrierAcc" placeholder="Carrier PCC" CssClass="form-control" runat="server" MaxLength="7"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="----Select Fare Type---" Selected="False"></asp:ListItem>

                                <asp:ListItem Value="NRM" Text="Normal Fare" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="--Select Status--" Selected="False"></asp:ListItem>
                                <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="ddProject" runat="server" CssClass="form-control">
                                <asp:ListItem Value="ALL" Text="Select Application" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="" Text="B2B"></asp:ListItem>
                                <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="btn btn-danger" OnClientClick="return Check();" />

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-danger" OnClick="BtnSearch_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grid1" runat="server" AutoGenerateColumns="false"
                                CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="Grid1_RowCancelingEdit"
                                OnRowEditing="Grid1_RowEditing" OnRowUpdating="Grid1_RowUpdating" OnRowDeleting="OnRowDeleting" OnRowDataBound="OnRowDataBound" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging">
                                <Columns>

                                    <asp:TemplateField HeaderText="Trip">
                                        <ItemTemplate>
                                            <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                            <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("Trip") %>' Style="text-wrap: inherit;"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Airline">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResultFrom" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Provider">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Corporate_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCorporateID" runat="server" Text='<%#Eval("CorporateID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGrdCorporateId" runat="server" Text='<%#Eval("CorporateID") %>' Width="100px" MaxLength="50" BackColor="#ffff66"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UserID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGrdUserId" runat="server" Text='<%#Eval("UserID") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password">
                                        <ItemTemplate>

                                            <asp:Label ID="lblPassword" runat="server" Text='******'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGrdPWD" runat="server" Text='<%#Eval("Password") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="CarrierAcc">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCarrierAcc" runat="server" Text='<%#Eval("CarrierAcc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGrdPCC" runat="server" Text='<%#Eval("CarrierAcc") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Style="text-wrap: inherit;"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlGrdStatus" runat="server" Width="150px" DataValueField='<%#Eval("Status")%>' SelectedValue='<%#Eval("Status")%>'>
                                                <%-- <asp:ListItem Value="1" Text="ACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="DEACTIVE"></asp:ListItem>--%>
                                                <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fare_Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCrdType" runat="server" Text='<%#Eval("CrdType") %>' Style="text-wrap: inherit;"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CreatedDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>' Style="text-wrap: inherit;"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EDIT">
                                        <ItemTemplate>
                                            <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                CssClass="newbutton_2" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                            <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                CssClass="newbutton_2" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                <EmptyDataTemplate>No records found</EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                    <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if (Provider == "1G" || Provider == "1GINT") {
                $("#divCorporateID").show();
                $("#divCarrierPCC").show();
                $("#divResultFrom").hide();

                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();
            }
            else if (Provider == "G8") {
                $("#divCorporateID").show();
                $("#divCarrierPCC").hide();
                $("#divResultFrom").show();

                //$("#DivRow").show();
                $("#DivLoginID").show();
                $("#DivLoginPwd").show();

            }
            else {
                $("#divCorporateID").show();
                $("#divCarrierPCC").hide();
                $("#divResultFrom").show();
                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();

            }
        }

        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_ddProject").val() == "ALL") {
                alert("Select Application Type.");
                $("#ctl00_ContentPlaceHolder1_ddProject").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select provider.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
                alert("Enter Corporate ID.");
                $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
                alert("Enter User ID.");
                $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
                alert("Enter Password.");
                $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").val() == "") {
                alert("Enter Carrier PCC.");
                $("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").focus();
                return false;
            }
        }

    </script>

</asp:Content>
