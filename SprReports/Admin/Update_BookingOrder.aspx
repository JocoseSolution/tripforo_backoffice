﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Update_BookingOrder.aspx.vb"
    Inherits="Update_BookingOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 08 || charCode == 46 || charCode == 32)) {
                return true;
            }
            else {
                return false;
            }
        }

        function validateptk() {
            var ddltype = document.getElementById("typedd");
            if (ddltype.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select Status Before Processing To Hold PNR!");
                return false;
            }
        }

    </script>
     <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>

</head>


<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;
        </div>
        <div align="center">
            <table width="90%">
                <tr>
                    <td align="right" style="font-weight: bold; font-size: 15px; color: #004b91;">Booking Reference No.
                    </td>
                    <td id="tdRefNo" runat="server" align="left" style="font-weight: bold;"></td>
                </tr>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </table>


             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Agency Details
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grd_AgencyDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Agent Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser_Id" runat="server" Text='<%#Eval("User_Id")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtUser_Id" Width="80px" runat="server" Text='<%#Eval("User_Id")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agency Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAgency_Name" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAgency_Name" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtEmail" Width="80px" runat="server" Text='<%#Eval("Email")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMobile" runat="server" Text='<%#Eval("Mobile")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMobile" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("Mobile")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAddress" Width="80px" runat="server" Text='<%#Eval("Address")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Address1")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txAddress1" Width="60px" runat="server" Text='<%#Eval("Address1")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Crd_Limit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAval_Crd_Limit" runat="server" Text='<%#Eval("Crd_Limit")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCrd_Limit" Width="50px" runat="server" Text='<%#Eval("Crd_Limit")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress4" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </td>
                </tr>

            </table>


            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">PNR Details
                    </td>
                </tr>
            
                <tr>


                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GvFlightHeader" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Booking Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookingDate" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Gds Pnr">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGdsPnr" runat="server" Text='<%#Eval("GDsPnr")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGdsPnr" runat="server" Width="70px" Text='<%#Eval("GDsPnr")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Airline Pnr">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAirlinePnr" runat="server" Width="70px" Text='<%#Eval("AirlinePnr")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtStatus" runat="server" Width="80px" Text='<%#Eval("Status")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                         <asp:BoundField HeaderText="Modify Status" DataField="MordifyStatus"></asp:BoundField>
                                         <asp:BoundField HeaderText="SearchId" DataField="SearchId"></asp:BoundField>
                                            <asp:BoundField HeaderText="BookingId" DataField="PNRId"></asp:BoundField>
                                            <asp:BoundField HeaderText="TicketId" DataField="TicketId"></asp:BoundField>
                                            <asp:BoundField HeaderText="SearchIdName" DataField="SearchIdName"></asp:BoundField>
                                        <asp:BoundField HeaderText="Hand_Bag_Fare" DataField="IsBagfares"></asp:BoundField>
                                        <asp:BoundField HeaderText="RESULTTYPE" DataField="RESULTTYPE"></asp:BoundField>
                                         <asp:TemplateField HeaderText="Message">
                                        <ItemTemplate>
                                            <asp:Label ID="lblmsg" runat="server"  ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="PaymentMode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPayment_Access" runat="server" Text='<%#Eval("Payment_Access")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPayment_Access" runat="server" Width="80px" Text='<%#Eval("Payment_Access")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="newbutton_2" CommandName="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="newbutton_2" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdateFltHeader" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>


                </tr>

            </table>



             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Gst Details
                    </td>
                </tr>
            
                <tr>


                    <td>
                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="grd_GST" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="GSTNO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGSTNO" runat="server" Text='<%#Eval("GSTNO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GST_Company_Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGST_Company_Name" runat="server" Text='<%#Eval("GST_Company_Name")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGST_Company_Name" runat="server" Width="70px" Text='<%#Eval("GST_Company_Name")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GST_Company_Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGST_Company_Address" runat="server" Text='<%#Eval("GST_Company_Address")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGST_Company_Address" runat="server" Width="70px" Text='<%#Eval("GST_Company_Address")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GST_PhoneNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGST_PhoneNo" runat="server" Text='<%#Eval("GST_PhoneNo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGST_PhoneNo" runat="server" Width="80px" Text='<%#Eval("GST_PhoneNo")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                         <asp:BoundField HeaderText="GST_Email" DataField="GST_Email"></asp:BoundField>
                                         <asp:BoundField HeaderText="GSTRemark" DataField="GSTRemark"></asp:BoundField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="newbutton_2" CommandName="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="newbutton_2" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress6" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>


                </tr>

            </table>


            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Passenger Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GvTravellerInformation" runat="server" AutoGenerateColumns="false"
                                    Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("Title")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFname" Width="100px" runat="server" Text='<%#Eval("FName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtLname" Width="100px" runat="server" Text='<%#Eval("LName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("PaxType")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTktNo" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTktNo" runat="server" Width="80px" Text='<%#Eval("TicketNumber")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server" Text='<%#Eval("MORDIFYSTATUS")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText ="Message">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server"  ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>

            </table>


            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Flight Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GvFlightDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFltId" runat="server" Text='<%#Eval("FltId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DepCity">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepcityName" runat="server" Text='<%#Eval("DepCityOrAirportName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDepcityName" Width="80px" runat="server" Text='<%#Eval("DepCityOrAirportName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DCityCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepCityOrAirportCode" runat="server" Text='<%#Eval("DepCityOrAirportCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDepcityCode" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("DepCityOrAirportCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ArrCity">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrCityOrAirportName" runat="server" Text='<%#Eval("ArrCityOrAirportName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtArrcityName" Width="80px" runat="server" Text='<%#Eval("ArrCityOrAirportName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ACityCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrCityOrAirportCode" runat="server" Text='<%#Eval("ArrCityOrAirportCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtArrcityCode" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("ArrCityOrAirportCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Airline">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("AirlineName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAirlineName" Width="80px" runat="server" Text='<%#Eval("AirlineName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AirCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirlineCode" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAirlineCode" Width="60px" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FltNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFltNumber" runat="server" Text='<%#Eval("FltNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFltNo" Width="50px" runat="server" Text='<%#Eval("FltNumber")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DepDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepDate" runat="server" Text='<%#Eval("DepDate")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDepDate" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("DepDate")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DepTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepTime" runat="server" Text='<%#Eval("DepTime")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDepTime" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("DepTime")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ArrTime">
                                            <ItemTemplate>
                                                <asp:Label ID="lblArrTime" runat="server" Text='<%#Eval("ArrTime")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtArrTime" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("ArrTime")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AirCraft">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirCraft" runat="server" Text='<%#Eval("AirCraft")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAirCraft" Width="50px" runat="server" Text='<%#Eval("AirCraft")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="FareBasis">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFareBasis" runat="server" Text='<%#Eval("AdtFareBasis")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFareBasis" Width="50px" runat="server" Text='<%#Eval("AdtFareBasis")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="RBD">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRBD" runat="server" Text='<%#Eval("AdtRbd")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRBD" Width="50px" runat="server" Text='<%#Eval("AdtRbd")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Cabin">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCabin" runat="server" Text='<%#Eval("Cabin")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCabin" Width="50px" runat="server" Text='<%#Eval("Cabin")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>



                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" Width="100px" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdateFlight" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </td>
                </tr>

            </table>



            
             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Meal & Baggage
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grd_Mealbagg" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="MealCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMealCode" runat="server" Text='<%#Eval("MealCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMealCode" Width="80px" runat="server" Text='<%#Eval("MealCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MealPrice">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMealPrice" runat="server" Text='<%#Eval("MealPrice")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMealPrice" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("MealPrice")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BaggageCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaggageCode" runat="server" Text='<%#Eval("BaggageCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBaggageCode" Width="80px" runat="server" Text='<%#Eval("BaggageCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BaggagePrice">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaggagePrice" runat="server" Text='<%#Eval("BaggagePrice")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBaggagePrice" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("BaggagePrice")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AirLineCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirLineCode" runat="server" Text='<%#Eval("AirLineCode")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAirLineCode" Width="80px" runat="server" Text='<%#Eval("AirLineCode")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BaggageDesc">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaggageDesc" runat="server" Text='<%#Eval("BaggageDesc")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBaggageDesc" Width="60px" runat="server" Text='<%#Eval("BaggageDesc")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BaggageCategory">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaggageCategory" runat="server" Text='<%#Eval("BaggageCategory")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBaggageCategory" Width="50px" runat="server" Text='<%#Eval("BaggageCategory")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BaggagePriceWithNoTax">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBaggagePriceWithNoTax" runat="server" Text='<%#Eval("BaggagePriceWithNoTax")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBaggagePriceWithNoTax" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("BaggagePriceWithNoTax")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MealDesc">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMealDesc" runat="server" Text='<%#Eval("MealDesc")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMealDesc" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealDesc")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MealCategory">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMealCategory" runat="server" Text='<%#Eval("MealCategory")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMealCategory" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealCategory")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                               <asp:TemplateField HeaderText="MealPriceWithNoTax">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMealPriceWithNoTax" runat="server" Text='<%#Eval("MealPriceWithNoTax")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtMealPriceWithNoTax" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealPriceWithNoTax")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress5" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </td>
                </tr>

            </table>


             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">LedgerDetails
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="Grd_LedgerDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="PnrNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPnrNo" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPnrNo" Width="80px" runat="server" Text='<%#Eval("PnrNo")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TicketNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketNo" runat="server" Text='<%#Eval("TicketNo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTicketNo" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("TicketNo")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TicketingCarrier">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketingCarrier" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTicketingCarrier" Width="80px" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ExecutiveID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExecutiveID" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtExecutiveID" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Debit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDebit" runat="server" Text='<%#Eval("Debit")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDebit" Width="80px" runat="server" Text='<%#Eval("Debit")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Credit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCredit" Width="60px" runat="server" Text='<%#Eval("Credit")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Aval_Balance">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAval_Balance" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAval_Balance" Width="50px" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookingType" runat="server" Text='<%#Eval("BookingType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBookingType" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("BookingType")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remark">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("Remark")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DISTRID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDISTRID" runat="server" Text='<%#Eval("DISTRID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDISTRID" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("DISTRID")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </td>
                </tr>

            </table>



          

            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Fare Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFareInformation" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <div>
                &nbsp;
            </div>
             <div id="ddtypediv" runat="server">
                  <div><b>Status:</b><asp:DropDownList ID="typedd" runat="server" CssClass="combobox">
                                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="Confirm" Text="Confirm"></asp:ListItem>
                                                                </asp:DropDownList> <br/>
                                                      
               </div>
              <asp:Button ID="Status" runat="server" OnClientClick="return validateptk();" Text="Booking on Hold" Style="width:150px;height:50px;padding:4px;margin-left:75px;" />
                 <br /><br />
                 <span id="MessageHold" runat="server"><b>Note:</b><br />
                     1.Change booking status Request to Hold(Booking show on Hold Request)
                     <br />
                     2.Balance deduct from agent A/C, in case Balance not deducted.
                     <br />
                     
                 </span>
            </div>
        </div>
    </form>
</body>
</html>





