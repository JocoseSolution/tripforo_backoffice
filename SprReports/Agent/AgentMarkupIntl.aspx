﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="AgentMarkupIntl.aspx.vb" Inherits="Reports_Agent_AgentMarkupIntl" %>
<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    
    
    

    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>

  

    <script type="text/javascript" src="../../js/chrome.js"></script>
     <div class="mtop80"></div>
    <div class="large-3 medium-3 small-12 columns">
        
                <uc1:settings runat="server" id="Settings" />
            
    </div>
    <div class="large-9 medium-8 small-12 columns">
        
            <div class="large-12 medium-12 small-12 bld blue">Agent Markup
            </div>
      
            <div class="clear1"></div>
       
            
                <div class="large-12 medium-12 small-12">
                    
                        <div >
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Text="Domestic" Value="D"></asp:ListItem>
                                <asp:ListItem Text="International" Selected="True" Value="I"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                   
                        
                            <div class="large-12 medium-12 small-12">
                                
                                    <div class="large-3 medium-3 small-3  columns">Agent ID:
                                    </div>
                                    <div class="large-3 medium-3 small-9 columns">
                                        <asp:TextBox ID="txt_AgentID" runat="server"  ReadOnly="True"></asp:TextBox>
                                    </div>
                                    <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">Airline Code:
                                    </div>
                                    <div class="large-3 medium-3 small-9  columns">
                                        <asp:DropDownList runat="server" data-placeholder="Choose a Airline..." TabIndex="2"
                                            ID="airline_code">
                                        </asp:DropDownList>
                                    </div>
                                    
                               
                                    <div class="large-3 medium-3 small-3 columns">Markup Type :
                                    </div>
                                    <div class="large-3 medium-3 small-9 columns">
                                        <asp:DropDownList ID="ddl_MarkupType" runat="server">
                                            <asp:ListItem Value="F" Selected="true">Fixed</asp:ListItem>
                                            <asp:ListItem Value="P" ForeColor="#999999">Percentage</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">Mark Up per pax:
                                    </div>
                                    <div class="large-3 medium-3 small-9 columns">
                                        <asp:TextBox ID="mk" runat="server" onKeyPress="return checkit(event)"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="mk" ErrorMessage="*"
                                            Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                    </div>
                                
                            </div>

                             <div class="clear1"></div>
                       
                        <div class="row">
                            <div class="large-12 medium-12 small-12">
                            <asp:UpdatePanel ID="UP" runat="server">
                                <ContentTemplate>

                                    <div class="large-12 medium-12 small-12">
                                        
                                            <div class="large-4 medium-4 small-6 medium-push-8 large-push-8 small-push-6 columns">
                                                <asp:Button ID="btnAdd" runat="server" Text="New Entry"  OnClientClick="return confirm('Are you sure you want to add this?');" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lbl" runat="server" Style="color: #CC0000;" Font-Bold="True"
                                                    Visible="False"></asp:Label>
                                                <asp:Label ID="lbl_msg" runat="server" Style="color: #CC0000;" Font-Bold="True"
                                                    Visible="False"></asp:Label>
                                            </div>

                                         <div class="clear1"></div>
                                       
                                            <div class="large-12 medium-12 small-12">
                                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                                    OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                                    OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" AllowPaging="True"
                                                    CssClass="GridViewStyle" GridLines="None" Width="100%">
                                                    <Columns>
                                                        <asp:CommandField ShowEditButton="True" />
                                                        <%-- <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />--%>
                                                        <asp:TemplateField HeaderText="Sr.No">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ID" runat="server" Text='<%#Eval("Counter") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="AirlineCode" HeaderText="AirlineCode" ControlStyle-CssClass="textboxflight1"
                                                            ReadOnly="true">
                                                            <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Trip" HeaderText="Trip" ControlStyle-CssClass="textboxflight1"
                                                            ReadOnly="true">
                                                            <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MarkupValue" HeaderText="Mark Up" ControlStyle-CssClass="textboxflight1">
                                                            <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="MarkupType" HeaderText="MarkUp Type" ControlStyle-CssClass="textboxflight1">
                                                            <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                        </asp:BoundField>
                                                        <asp:CommandField ShowDeleteButton="True" />
                                                    </Columns>
                                                    <RowStyle CssClass="RowStyle" />
                                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                    <PagerStyle CssClass="PagerStyle" />
                                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                    <HeaderStyle CssClass="HeaderStyle" />
                                                    <EditRowStyle CssClass="EditRowStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </div>
                                       
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                   
                </div> </div>
            
    </div>

    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode


            if (!(charCode == 46 || charCode == 48 || charCode == 49 || charCode == 50 || charCode == 51 || charCode == 52 || charCode == 53 || charCode == 54 || charCode == 55 || charCode == 56 || charCode == 57 || charCode == 8)) {
                //                alert("This field accepts only Float Number.");
                return false;


            }

            status = "";
            return true;

        }


    </script>

</asp:Content>
