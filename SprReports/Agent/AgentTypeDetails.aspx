﻿<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false" CodeFile="AgentTypeDetails.aspx.vb" Inherits="SprReports_Agent_AgentTypeDetails" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>





<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Register</li>
                  <li class="breadcrumb-item active" aria-current="page">Group Type Details</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">

                        <div class="form-group">

                            <asp:TextBox CssClass="form-control" placeholder="Agent Type" runat="server" ID="TextBoxGroupType" oncopy="return false" onpaste="return false" MaxLength="20" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz0123456789');"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Group Type is Required." ControlToValidate="TextBoxGroupType" ValidationGroup="ins"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox CssClass="form-control" placeholder="Description" runat="server" ID="TextBoxDesc" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDesc" runat="server" ErrorMessage="Description is Required." ControlToValidate="TextBoxDesc" ValidationGroup="ins"></asp:RequiredFieldValidator>
                            <div class="col-md-3">
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="TextBoxDesc" ID="RegularExpressionValidator4" ValidationExpression="^[\s\S]{1,30}$" runat="server" ValidationGroup="ins" ErrorMessage="Maximum 50 characters Use"></asp:RegularExpressionValidator>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">

                            <asp:Button CssClass="btn btn-danger" ID="ButtonSubmit" runat="server" Text="Add GroupType" ValidationGroup="ins" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Label ID="LableMsg" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:UpdatePanel runat="server">

                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                CssClass="table" GridLines="None" Width="100%">
                                <Columns>
                                    <asp:CommandField ShowEditButton="True" ValidationGroup="VG" />
                                    <asp:TemplateField HeaderText="Group Type">

                                        <ItemTemplate>
                                            <asp:Label ID="LableGroupType" runat="server" Text='<%# Eval("GroupType")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="LableDesc" runat="server" Text='<%# Eval("Text")%>' Style="text-wrap: inherit;"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBoxDesc" TextMode="MultiLine" runat="server" Columns="100" Rows="2" Text='<%# Eval("Text")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ErrorMessage="Description is Required." ControlToValidate="TextBoxDesc" ValidationGroup="VG"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="TextBoxDesc" ValidationExpression="^[\s\S]{0,500}$" ErrorMessage="Please enter a maximum of 500 characters"></asp:RegularExpressionValidator>
                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:CommandField ShowDeleteButton="True" />

                                </Columns>
                            </asp:GridView>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_TextBoxGroupType").click(function () {

                $("#ctl00_ContentPlaceHolder1_LableMsg").hide();


            });
            $("#ctl00_ContentPlaceHolder1_TextBoxDesc").click(function () {

                $("#ctl00_ContentPlaceHolder1_LableMsg").hide();
            });
        });


        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

    </script>
</asp:Content>
