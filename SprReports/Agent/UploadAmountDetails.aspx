<%@ Page Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="UploadAmountDetails.aspx.vb" Inherits="Reports_Agent_UploadAmountDetails"
    Title="Upload Amount Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    
<%--    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

               
    <div class="content-wrapper">
                    <div class="page-header">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Account</li>
                  <li class="breadcrumb-item active" aria-current="page">Upload Amount Report</li>
                </ol>
              </nav>
       </div>
                     <div class="card">
                    <div class="card-body">

                        <div class="row">
                            
                           
                            
                             <div class="col-md-3">
                                  <div class="form-group">
                                    <input type="text" name="From" id="From" placeholder="From Date" class="txtCalander form-control" readonly="readonly"/>
                                      </div>
                                 </div>
                                   
                                <div class="col-md-3">
                               <div class="form-group">
                                    <input type="text" name="To" id="To" placeholder="To Date" class="txtCalander form-control" readonly="readonly"/>
                                    
                                </div>
                            </div>
                      
                                    <div class="col-md-3">
                               <div class="form-group">
                                    <asp:DropDownList ID="ddl_PType" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="--Select Payment Mode--" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                        <asp:ListItem Text="Cash Deposite In Bank" Value="Cash Deposite In Bank"></asp:ListItem>
                                        <asp:ListItem Text="NetBanking" Value="NetBanking"></asp:ListItem>
                                        <asp:ListItem Text="RTGS" Value="RTGS"></asp:ListItem>
                                    </asp:DropDownList>
                                   </div>
                                        </div>
                                
                            
                                    <div class="col-md-3">
                               <div class="form-group">
                                    <asp:DropDownList ID="ddl_status" runat="server"  CssClass="form-control">
                                        <asp:ListItem Text="--Select Status--" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                        <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                        <asp:ListItem Text="Confirm" Value="Confirm"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>

                                    </asp:DropDownList>
                                   </div>
                                        </div>
                              </div>
                          
                                <div class="row" id="td_Agency" runat="server">
                                   
                                                <div class="col-md-3" id="td_ag">
                                                    <div class="form-group">
                                                      
                                                            <input type="text" id="txtAgencyName" placeholder="Agency Name or ID" name="txtAgencyName"  onfocus="focusObj(this);" onblur="blurObj(this);"  class="form-control" />
                                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                                       
                                                    </div>

                                                </div>
                                         

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                <asp:DropDownList ID="DropDownListADJ" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="--Select Upload Type--" Value="Select" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Fresh Upload" Value="FU"></asp:ListItem>
                                                    <asp:ListItem Text="Adjustment" Value="AD"></asp:ListItem>
                                                </asp:DropDownList>
                                                    </div>
                                                </div>
                                     

                                       <div class="col-md-3">
                                           <div class="form-group">
                                    <asp:Button ID="btn_showdetails" runat="server" CssClass="btn btn-danger" Text="Search"/>
                                               </div>
                                </div>
                                </div>
                          
                         
                                <td id="tr_SearchType" runat="server" visible="false" colspan="4">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="fltdtls"
                                                align="left">Search Type</td>
                                            <td style="width: 200px">
                                                <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                    Text="Agent" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                                Text="Own" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                     
                            
                             
                          
                        </div>
                        </div>
                         
        <br />
        <br />

                     <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                         
                                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="grd_deposit" CssClass="table" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:BoundField HeaderText="Status" DataField="Status" />
                                            <asp:BoundField HeaderText="Transaction&nbsp;Date" DataField="Date" />
                                            <asp:BoundField HeaderText="Agency&nbsp;Name" DataField="AgencyName" />
                                            <asp:BoundField HeaderText="AgencyId" DataField="AgencyID" />
                                            <asp:BoundField HeaderText="Amount" DataField="Amount" />
                                            <asp:BoundField HeaderText="ModeOfPayment" DataField="ModeOfPayment" />
                                            <asp:BoundField HeaderText="Bank&nbsp;Name" DataField="BankName" />
                                            <asp:BoundField HeaderText="ChequeNo" DataField="ChequeNo" />
                                            <asp:BoundField HeaderText="Cheque&nbsp;Date" DataField="ChequeDate" />
                                            <asp:BoundField HeaderText="TransactionId" DataField="TransactionID" />
                                            <asp:BoundField HeaderText="BankAreaCode" DataField="BankAreaCode" />
                                            <asp:BoundField HeaderText="Deposit&nbsp;City" DataField="DepositCity" />
                                            <asp:BoundField HeaderText="Deposite&nbsp;Office" DataField="DepositeOffice" />
                                            <asp:BoundField HeaderText="Concern&nbsp;Person" DataField="ConcernPerson" />
                                            <asp:BoundField HeaderText="Reciept&nbsp;No" DataField="RecieptNo" />
                                            <asp:BoundField HeaderText="Remark" DataField="Remark" />
                                            
                                            <asp:BoundField HeaderText="Remark&nbsp;By&nbsp;Account" DataField="RemarkByAccounts" />
                                            <asp:BoundField HeaderText="Account&nbsp;Id" DataField="AccountID" />
                                            <%--<asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />--%>
                                        </Columns>

                                    </asp:GridView>
                             
                        </div>
                        </div>
                         </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

</asp:Content>
