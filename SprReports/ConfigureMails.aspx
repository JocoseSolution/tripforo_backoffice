﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfigureMails.aspx.vb" Inherits="SprReports_ConfigureMails" MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="Styles/tables.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />

     <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
         .overfl {
             overflow:auto;
         }
    </style>
    <div class="content-wrapper">
      
              <div class="card">
                    <div class="card-body">
                              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight</li>
                  <li class="breadcrumb-item active" aria-current="page">Configure Mails</li>
                </ol>
              </nav>

                        <div class="row">
                            <div class="col-md-3" style="display:none">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Module Type</label>--%>
                                    <asp:DropDownList ID="ddl_airline" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="">--Select Module Type--</asp:ListItem>
                                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                        <asp:ListItem Value="Reissue">Reissue</asp:ListItem>
                                        <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                        <asp:ListItem Value="Failed">Failed</asp:ListItem>
                                        <asp:ListItem Value="GroupBooking">GroupBooking</asp:ListItem>
                                   
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Module Type</label>--%>
                                    <asp:DropDownList ID="ddlModuleType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="">---Select Module Type---</asp:ListItem>
                                         <asp:ListItem Value="Hold">Hold</asp:ListItem>
                                        <asp:ListItem Value="Reissue">Reissue</asp:ListItem>
                                        <asp:ListItem Value="Refund">Refund</asp:ListItem>
                                        <asp:ListItem Value="Failed">Failed</asp:ListItem>
                                         <asp:ListItem Value="GroupBooking">GroupBooking</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                  <%--  <label for="exampleInputPassword1">To Email</label>--%>
                                    <asp:TextBox ID="txtToEmail" placeholder="To Email" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                       
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Is Active</label>
                                    <asp:RadioButton ID="rbtnIsActiveTrue" Text="Yes" runat="server" GroupName="grpIsAvtive" Checked="true" />
                                    <asp:RadioButton ID="rbtnIsActiveFalse" Text="No" runat="server" GroupName="grpIsAvtive" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">

                                    <asp:Button ID="btnConfigureMail" runat="server" Text="Submit" CssClass="btn btn-danger" OnClientClick="return configValidation();" />
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                        <br />
                        <br />

                        <div class="card">

                            <div class="card-body">

                                <div id="divReport" class="table-responsive" runat="server" visible="false">
                                    <%-- style="overflow: scroll; max-height: 250px; width: 100%; float: left;"--%>
                                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table table-hover" ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" GridLines="None" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sno" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Sno" runat="server" Text='<%#Eval("Sno")%>'></asp:Label>(View)</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Module">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Module" runat="server" Text='<%#Eval("ModuleType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txt_ToEmail" runat="server" Text='<%#Eval("ToEmail") %>' ></asp:TextBox>
                                              
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ToEmail" runat="server" Text='<%#Eval("ToEmail") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Active/Deactive">
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlActDact" runat="server" SelectedValue='<%#Eval("IsActive")%>'>
                                                        <asp:ListItem Value="True">Activate</asp:ListItem>
                                                        <asp:ListItem Value="False">Deactivate</asp:ListItem>
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_IsActive" runat="server" Text='<%#Eval("IsActive")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CreatedBy" runat="server" Text='<%#Eval("CreatedBy")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Created Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CreatedDate" runat="server" Text='<%#Eval("CreatedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_UpdatedBy" runat="server" Text='<%#Eval("UpdatedBy")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_UpdatedDate" runat="server" Text='<%#Eval("UpdatedDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lbtnUpdate" CssClass="newbutton_2" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Sno")%>' OnClientClick="return confirmUpdate(this);"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnCancel" CssClass="newbutton_2" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" CssClass="newbutton_2" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" CssClass="newbutton_2" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Sno")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle"/>
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    

    </div>
    <script type="text/javascript">
        function confirmUpdate(thisObj) {
            var txtEm;
            var objtoFoc;
            var cntEm = 0;
            var thisObjRow = $(thisObj).parent().parent();
            var txtEmObj = $($(thisObjRow).find("td")[1]).find("input[type='text']");

     
            if ($.trim($(txtEmObj).val()) == "") {
                alert("Email is required");
                $("#" + $.trim($(txtEmObj).attr("id"))).focus();
                return false;
            }
            else {
                if (/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test($.trim($(txtEmObj).val()))) {
                    //alert("hi")
                    var upd = confirm('Are you sure to update this configuration');
                    if (upd == true) {
                        return true;
                    }
                    else {
                        return false;
                    }
                    return true;
                }
                else {
                    alert("invalid email");
                    $("#" + $.trim($(txtEmObj).attr("id"))).focus();
                    return false;
                }
               
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function configValidation() {
            if ($.trim($("#<%=ddlModuleType.ClientID%>").val()) == "") {
                alert("Module type is required");
                $("#<%=ddlModuleType.ClientID%>").focus();
                                        return false;
            }

           

              if ($.trim($("#<%=txtToEmail.ClientID%>").val()) == "") {
                alert("Email are required");
                $("#<%=txtToEmail.ClientID%>").focus();
                                        return false;
                                    }

           
        }

    
    </script>
   
</asp:Content>
