﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="AddBankDetails.aspx.vb" Inherits="SprReports_Distr_AddBankDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type="text"], input[type="password"], select
        {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>
    <table style="margin: 0 auto; width: 100%;">
        <tr>
            <td>
                <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
                    <tr>
                        <td style="background: #fff;">
                            <table width="800px" cellpadding="0" cellspacing="3" style="background: #fff;" align="center">
                                <tr>
                                    <td align="left" style="color: #004b91; font-size: 13px; font-weight: bold">
                                        &nbsp; Add Bank Details
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <table width="800" border="0" cellspacing="10" cellpadding="2" align="center">
                                            <tr>
                                                <td style="font-weight: bold">
                                                    Bank Name :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_bankname" runat="server"></asp:TextBox>
                                                </td>
                                                <td align="left" style="font-weight: bold">
                                                    Branch Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_branchname" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">
                                                    Area :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_area" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="font-weight: bold">
                                                    Account No:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_accno" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight: bold">
                                                    IFSC/NEFT Code :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txt_code" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="BlockAirlineUpdPanel" runat="server">
                 <ContentTemplate>
             <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" Width="80%" ID="GrdMarkup" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" AllowPaging="true" PageSize="100" onpageindexchanging="GrdMarkup_PageIndexChanging">
            <Columns>   
                <%--<asp:BoundField DataField="AgentID" HeaderText="AGENT ID" ReadOnly="true" />
                <asp:BoundField DataField="HtlType" HeaderText="TRIP TYPE"  ReadOnly="true" />
                <asp:BoundField DataField="Country" HeaderText="COUNTRY"  ReadOnly="true" />
                <asp:BoundField DataField="City" HeaderText="CITY"  ReadOnly="true" />
                <asp:BoundField DataField="Star" HeaderText="STAR"  ReadOnly="true"  />
                <asp:BoundField DataField="MarkupType" HeaderText="MARKUP TYPE"  ReadOnly="true"  />--%>
                <asp:TemplateField HeaderText="Bank Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBankName" runat="server" Text='<%# Bind("BankName") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblBankName" runat="server" Text='<%#Eval("BankName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Branch Name">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBranchName" runat="server" Text='<%# Bind("BranchName") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblBranchName" runat="server" Text='<%#Eval("BranchName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Area">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtArea" runat="server" Text='<%# Bind("Area") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblArea" runat="server" Text='<%#Eval("Area")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AccountNumber">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtAccountNumber" runat="server" Text='<%# Bind("AccountNumber") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NEFTCode">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtNEFTCode" runat="server" Text='<%# Bind("NEFTCode") %>'  Width="83px"  MaxLength="200"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblNEFTCode" runat="server" Text='<%#Eval("NEFTCode")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate"  ReadOnly="true"  />
                 <asp:TemplateField HeaderText="EDIT">
                 <ItemTemplate>
                        <asp:ImageButton ID="imgbtnEdit" runat="server" CausesValidation="False" CommandName="Edit"  Text="EDIT"
                            ImageUrl="~/Images/edit.png" ToolTip="Edit" Height="16px" Width="20px" ></asp:ImageButton>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:ImageButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" Text="UPDATE"
                            ImageUrl="~/Images/ok.png" ToolTip="Update" Height="16px" Width="20px" >
                         </asp:ImageButton>
                        <asp:ImageButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="CANCEL" ToolTip="Cancel" Height="16px" Width="20px"
                             ForeColor="#20313f" Font-Strikeout="False" Font-Overline="False" Font-Bold="true" ImageUrl="../../Images/cancel1.png" ></asp:ImageButton>
                    </EditItemTemplate>
                </asp:TemplateField>                         
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblCounter" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="DELETE">
                 <ItemTemplate>
                        <asp:ImageButton ID="lbkDelete" runat="server" CausesValidation="True" CommandName="Delete" Text="DELETE" ToolTip="Delete" Height="16px" Width="16px"
                            ImageUrl="../../Images/delete.png"   OnClientClick="return confirm('Are you sure to delete it?');"></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                  <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
        </asp:GridView>                                    
         </ContentTemplate>
            </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
