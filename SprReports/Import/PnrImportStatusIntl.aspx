﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="PnrImportStatusIntl.aspx.vb" Inherits="Reports_Import_PnrImportStatusIntl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        #wrapper {
            width: 100% !important;
            padding: 0 10px;
            margin: 38px auto !important;
        }
    </style>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>

    <div class="content-wrapper">

        <div class="card">

            <div class="card-body">


                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight</li>
                  <li class="breadcrumb-item active" aria-current="page">Search International Import Booking</li>
                       
                 </ol>
              </nav>
                <div class="row">
                    <div class="col-md-3">

                        <div class="form-group">
                            <input type="text" name="From" id="From" placeholder="From Date" class="form-control"
                                readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="To" id="To" placeholder="To Date" class="form-control" readonly="readonly" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_PNR" class="form-control" runat="server" placeholder="PNR"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">


                            <asp:TextBox ID="txt_OrderId" class="form-control" runat="server" placeholder="Order ID"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="Airline" class="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" id="td_Agency" runat="server">
                           

                                <input
                                    type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" class="form-control" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                           
                        </div>
                    </div>
                    </div>

                    <div id="tr_ExecID" class="row" runat="server" >
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:DropDownList ID="ddl_ExecID" class="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:DropDownList ID="ddl_Status" class="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-danger" ToolTip="Export by Date" />&nbsp;&nbsp;   
                        </div>
                    </div>
                </div>
                
                 <div ID="Label1" class="alert_msg info_msg fl">
                    <b class="status_info fl">Note: </b><span class="status_cont"> To get Today's booking without above parameter,do not fill any field,only click on search your booking.</span>
                </div>
                
            </div>
        </div>
        <br />
        <br />
        <div class="card">

            <div class="card-body">
                <div class="table-responsive">
                    <asp:UpdatePanel ID="UP" runat="server">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridImportProxyDetail" runat="server" AutoGenerateColumns="False"
                                Width="100%" CssClass="GridViewStyle" PageSize="30" AllowPaging="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <a id="ancher" runat="server"
                                                target="_blank" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #800000; font-weight: bold;"></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OrderId">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_OrderId" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PNR">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PnrNo" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AgentID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AgentID" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ag_Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Ag_Name" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sector">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Depart" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_DDate" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Alert Message">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AlertMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Status" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Executive ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Exec" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_ReqTime" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_AcceptedDate" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_UpTime" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
