﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Branch.aspx.cs" Inherits="SprReports_PrivilegePanel_Branch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
  <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Privilege Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">Create Branch</li>
                </ol>
              </nav>
  
                  
                        <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <asp:TextBox CssClass="form-control" placeholder="Branch" runat="server" ID="Branchtxt" name="branch"  MaxLength="30"></asp:TextBox>
                            </div>
                            </div>

                             <div class="col-md-3">
                           <div class="form-group">
                         
                                <asp:Button ID="Submit" runat="server"  Text="Submit" CssClass="btn btn-danger" OnClick="Submit_Click" ValidationGroup="group2"  />
                            </div>
                        </div>

                             


                       
                              <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>

                            </div>
                        </div>
                              </div>
                              </div>
                              </div>
      <br />
      <br />
       <div class="card">
            <div class="card-body">
                <div class="table-responsive">





                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>

                                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Branch_id"
                                        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                        CssClass="table" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Branch" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBranch" runat="server" MaxLength="30" Text='<%# Eval("Branch") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                  
                                          <%--  <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"  ItemStyle-Width="150" />--%>

                                              <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                            <EditItemTemplate>
                                <asp:LinkButton ID="lbtnUpdate" CssClass="newbutton_2" runat="server" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Branch_id")%>' OnClientClick="return confirmUpdate(this.id);"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnCancel" CssClass="newbutton_2" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" CssClass="newbutton_2" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" CssClass="newbutton_2" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Branch_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                    </div>

                </div>
            </div>
     


 

    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Branchtxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
    </script>

      <script type="text/javascript">
          $(document).ready(function () {
              $("#ctl00_ContentPlaceHolder1_Branchtxt").click(function () {

                  $("#ctl00_ContentPlaceHolder1_Label1").hide();


              });
           
          });
</script>
  

    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {
             
                if ($.trim($("#ctl00_ContentPlaceHolder1_Branchtxt").val()) == "") {
                    alert("Please select Role ");
                    $("#<%=Branchtxt.ClientID%>").focus();
                    return false;
                  }
                
            });
        });
      
     
        //function confirmUpdate(thisObj) {
                  
        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").val()) == "") {
             
        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").focus();
        //        return false;
        //    }
        //    else {
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}

        function confirmUpdate(aaa) {
          
            var txtid = aaa.replace("lbtnUpdate", "txtBranch");
           
            if ($.trim($("#" + txtid).val()) == "") {

                $("#" + txtid).focus();
                return false;
            }
            else {
                var upd = confirm('Are you sure to update this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }         

    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

