﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="ExportPermission.aspx.cs" Inherits="ExportPermission" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>




    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Privilege Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">Export Permission</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label id="useridlb"></label>
                            <asp:DropDownList ID="UserID" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        </div>
                  
                    <div class="col-md-3">
                        <div class="form-group">
                            <label id="ddlRole_typelb"></label>
                            <asp:DropDownList ID="ddlRole_type" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label id="StatusTypelb"></label>
                            <asp:DropDownList ID="StatusType" runat="server" CssClass="form-control" >
                             <asp:ListItem Value="0" Text="---Select Status---"></asp:ListItem>

                                <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


             
                    <div class="col-md-3">
                        <div class="form-group">
                                                        <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>

                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-danger" OnClick="Submit_Click" ValidationGroup="group1" />


                        </div>
                    </div>
                </div>
                </div>
                </div>
        <br />
        <br />

         <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="ID"
                    OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                    OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                    CssClass="table" GridLines="Both" Width="100%">

                    <Columns>
                        <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("UserID") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="RoleType" ItemStyle-Width="150" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblRoleType" runat="server" Text='<%# Eval("RoleType") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="CreatedDate" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="CreatedBy" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedBy") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="UpdateDate" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("UpdateDate") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="UpdateBy" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblUpdateBy" runat="server" Text='<%# Eval("UpdateBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    

                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                                    <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                    <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                </asp:DropDownList>

                            </EditItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                            <EditItemTemplate>
                                <asp:LinkButton ID="lbtnUpdate" CssClass="newbutton_2" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                <asp:LinkButton ID="lbtnCancel" CssClass="newbutton_2" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" CssClass="newbutton_2" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" CssClass="newbutton_2" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>

            </div>
        </div>
    </div>
    </div>
        
</asp:Content>

