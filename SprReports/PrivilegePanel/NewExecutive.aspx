﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="NewExecutive.aspx.cs" Inherits="NewExecutive" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;
        }

        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
    <script src="validation.js"></script>



    <script type="text/javascript">
        $(document).ready(function () {


            $('#<%=Submit.ClientID%>').click(function (event) {

                var returntypp = true;
                if ($.trim($("#<%=txtemail.ClientID%>").val()) == "") {

                    $("#<%=txtemail.ClientID%>").focus();
                    $('.error').show();
                    returntypp = false;
                }
                else {
                    $('.error').hide();
                }
                if ($.trim($("#<%=txtpassword.ClientID%>").val()) == "") {

                    $("#<%=txtpassword.ClientID%>").focus();
                    $('.error1').show();
                    returntypp = false;
                }
                else {
                    $('.error1').hide();
                }
                if ($.trim($("#<%=txtname.ClientID%>").val()) == "") {

                    $("#<%=txtname.ClientID%>").focus();
                    $('.error2').show();
                    returntypp = false;
                }
                else {
                    $('.error2').hide();
                }

                if ($.trim($("#<%=txtmobile.ClientID%>").val()) == "") {

                    $("#<%=txtmobile.ClientID%>").focus();
                    $('.error3').show();
                    returntypp = false;
                }
                else {
                    $('.error3').hide();
                }

                if ($.trim($("#<%=ddlRole_type.ClientID%>").val()) == "0") {

                    $("#<%=ddlRole_type.ClientID%>").focus();
                    $('.error4').show();
                    returntypp = false;
                }
                else {
                    $('.error4').hide();
                }
                return returntypp;
            });

        });

        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>



    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Privilege Setting</li>
                  <li class="breadcrumb-item active" aria-current="page">New Executive</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="lblemail" runat="server"></label>
                            <asp:TextBox CssClass="form-control" placeholder="Email" runat="server" ID="txtemail" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="txtemail" ErrorMessage="*"
                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtemail" ValidationGroup="group1" ErrorMessage=" enter valid email "
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator>


                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="lblpassword" runat="server"></label>
                            <asp:TextBox CssClass="form-control" runat="server" placeholder="Password" ID="txtpassword" MaxLength="40" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVMK1" runat="server" ControlToValidate="txtpassword" ErrorMessage="*"
                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="txtpassword" ValidationGroup="group1" Display="dynamic" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{5,}$" ErrorMessage="Minimum 5 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="lblname" runat="server"></label>
                            <asp:TextBox CssClass="form-control" placeholder="Name" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" runat="server" ID="txtname" MaxLength="20"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVMK2" runat="server" ControlToValidate="txtname" ErrorMessage="*"
                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtname" ID="RegularExpressionValidator1" ValidationExpression="^[\s\S]{5,30}$" runat="server" ErrorMessage="Minimum 5 and Maximum 20 characters required."></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtname" ValidationGroup="group1"
                                ValidationExpression="[a-zA-Z ]*$" ErrorMessage="*Valid characters: Alphabets and space." />

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="lblmobile" runat="server"></label>
                            <asp:TextBox CssClass="form-control" placeholder="Mobile No." onkeypress="return checkitt(event)" runat="server" ID="txtmobile" MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtmobile" ErrorMessage="*"
                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="group1"
                                ControlToValidate="txtmobile" ErrorMessage="Please Fill valid 10 digit mobile no."
                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                        </div>
                    </div>



                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1" id="lblrole_type"></label>
                            <asp:DropDownList CssClass="form-control" ID="ddlRole_type" runat="server">
                            </asp:DropDownList>
                        </div>

                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1" id="lblBranch"></label>
                            <asp:DropDownList CssClass="form-control" ID="DD_Branch" runat="server">
                            </asp:DropDownList>
                        </div>

                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>

                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-danger" OnClick="Submit_Click" ValidationGroup="group1" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="user_id"
                        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                        CssClass="table" GridLines="None" Width="100%">

                        <Columns>
                            <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblpwd" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtExecPwd" runat="server" Text='<%#Eval("password") %>' Width="100px" BackColor="#ffff66" MaxLength="49"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlRole_Name" runat="server" Width="150px"></asp:DropDownList>
                                    <asp:Label ID="lblRoleNameHidden" runat="server" Text='<%# Eval("Role") %>' Visible="false"></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblroletype" runat="server" Text='<%# Eval("role_type") %>'></asp:Label>
                                </ItemTemplate>
                                <%--<EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Type" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleTypeHidden" runat="server" Text='<%# Eval("role_type") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblemailid" runat="server" Text='<%# Eval("Email_id") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile_no" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblmobileno" runat="server" Text='<%# Eval("mobile_no") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>

                                    <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                                        <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                    </asp:DropDownList>

                                </EditItemTemplate>

                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CssClass="newbutton_2" CommandName="Update" Text="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CssClass="newbutton_2" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="newbutton_2" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="newbutton_2" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("user_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

