﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="Role.aspx.cs" Inherits="Login" %>

<%@ Register Src="~/UserControl/PageAllow1.ascx" TagPrefix="uc1" TagName="PageAllow1" %>










<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Privilege Setting</li>
                  <li class="breadcrumb-item active" aria-current="page"> Role Page</li>
                </ol>
              </nav>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox CssClass="form-control" placeholder="Role" runat="server" ID="Roletxt" name="Role" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList CssClass="form-control" ID="RoleType" runat="server">
                                <asp:ListItem Text="--select Role---" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Executive" Value="EXEC"></asp:ListItem>
                                <asp:ListItem Text="Agent" Value="AGENT"></asp:ListItem>
                                <asp:ListItem Text="Admin" Value="ADMIN"></asp:ListItem>
                                <asp:ListItem Text="Sales" Value="SALES"></asp:ListItem>
                                <asp:ListItem Text="Account" Value="ACC"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">

                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-danger" OnClick="Submit_Click" ValidationGroup="group2" />
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>

                        </div>
                    </div>
                </div>

            </div>
            </div>
      <br />
            <br />
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>

                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Role_id"
                                    OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                    OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                    CssClass="table" GridLines="None" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Role" ItemStyle-Width="150">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRole" runat="server" MaxLength="30" Text='<%# Eval("Role") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRole_type" runat="server" Text='<%# Eval("Role_Type") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true"  ItemStyle-Width="150" />--%>

                                        <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="lbtnUpdate" runat="server" CssClass="newbutton_2" CommandName="Update" Text="Update" CommandArgument='<%#Eval("Role_id")%>' OnClientClick="return confirmUpdate(this.id);"></asp:LinkButton>
                                                <asp:LinkButton ID="lbtnCancel" runat="server" CssClass="newbutton_2" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="newbutton_2" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="newbutton_2" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Role_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
            </div>

        </div>
    </div>
  




    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_Submit').click(function (event) {

                var data = $("#ContentPlaceHolder1_Roletxt").val();


                var length = data.length;

                if (length < 1) {

                    $('.error').show();
                    return false;
                }
                else {
                    $('.error').hide();
                }


            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Roletxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });
            $("#ctl00_ContentPlaceHolder1_RoleType").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
        });
    </script>


    <script type="text/javascript">

        $(document).ready(function () {

            $('#<%=Submit.ClientID%>').click(function (event) {

                if ($.trim($("#ctl00_ContentPlaceHolder1_Roletxt").val()) == "") {
                    alert("Please select Role ");
                    $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }

                if ($.trim($("#<%=RoleType.ClientID%>").val()) == "0") {
                    alert("Please select Role Type");
                    $("#<%=RoleType.ClientID%>").focus();
                    return false;
                }


            });
        });


        //function confirmUpdate(thisObj) {

        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").val()) == "") {

        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl06_txtRole").focus();
        //        return false;
        //    }
        //    else {
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}

        function confirmUpdate(aaa) {

            var txtid = aaa.replace("lbtnUpdate", "txtRole");

            if ($.trim($("#" + txtid).val()) == "") {

                $("#" + txtid).focus();
                return false;
            }
            else {
                var upd = confirm('Are you sure to update this configuration');
                if (upd == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

