﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="RejectedTicket.aspx.vb" Inherits="SprReports_RejectedTicket" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />


    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight</li>
                  <li class="breadcrumb-item active" aria-current="page"> Search Flight Rejected Ticket</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="From" placeholder="From Date" id="From" class="form-control"
                                readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="text" name="To" id="To" placeholder="To Date" class="form-control" readonly="readonly" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="PNR" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_OrderId" placeholder="Order ID" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_PaxName" placeholder="Pax Name" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:DropDownList ID="ddlTrip" runat="server" CssClass="form-control">
                                <asp:ListItem Text="--Select Trip--" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Domestic" Value="D"></asp:ListItem>
                                <asp:ListItem Text="International" Value="I"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_TktNo" placeholder="Ticket No" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:TextBox ID="txt_AirPNR" placeholder="AirLine" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                    </div>
                    <div class="col-md-3" id="td_Agency" runat="server">
                        <div class="form-group">
                            <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" value="Agency Name or ID" />
                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                        </div>
                    </div>

                    <div class="col-md-3" id="divPaymentMode" runat="server">
                        <label for="exampleInputEmail1">PaymentMode :</label>
                        <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                            <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3" id="divPartnerName" runat="server">
                        <label for="exampleInputEmail1">PartnerName :</label>
                        <asp:DropDownList CssClass="form-control" ID="txtPartnerName" runat="server">
                        </asp:DropDownList>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-danger" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_export" runat="server" CssClass="btn btn-danger" Text="Export" />
                        </div>
                    </div>

                </div>



                <div class="alert_msg info_msg fl">
                    <b class="status_info fl">Note: </b><span class="status_cont">* N.B: To get Today's booking without above parameter,do not fill any field, only click on search your booking.</span>

                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive" id="divReport" runat="server" visible="true">
                    <div class="col-md-12">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                            <ContentTemplate>
                                <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found" ID="ticket_grdview" runat="server" AllowPaging="True"
                                    AutoGenerateColumns="False" CssClass="table"
                                    GridLines="None" Width="100%" PageSize="30">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Order ID">
                                            <ItemTemplate>
                                                <%-- <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>
                                                <a data-toggle="modal" data-target="#myModal" href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe"
                                                    rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                    <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Pnr">
                                            <ItemTemplate>
                                                <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgencyId">
                                            <ItemTemplate>
                                                <asp:Label ID="AgentID" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UserId">
                                            <ItemTemplate>
                                                <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Executive ID">
                                            <ItemTemplate>
                                                <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Rejected Remark">
                                            <ItemTemplate>
                                                <asp:Label ID="RejectRemark" runat="server" Text='<%#Eval("RejectedRemark")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="AirLine">
                                            <ItemTemplate>
                                                <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Partner Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                        <asp:BoundField HeaderText="Trip" DataField="trip"></asp:BoundField>
                                        <asp:BoundField HeaderText="Net Fare" DataField="TotalAfterDis">
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Convenience Fee">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PGcharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                        <asp:BoundField HeaderText="Booking Date" DataField="CreateDate"></asp:BoundField>
                                        <asp:BoundField HeaderText="Rejected Date" DataField="RejectDate"></asp:BoundField>

                                        <asp:TemplateField HeaderText="Payment Mode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
