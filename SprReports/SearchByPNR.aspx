﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="SearchByPNR.aspx.cs" Inherits="SprReports_SearchByPNR" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%-- <link href="<%=ResolveUrl("~/CSS/main2.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/style.css")%>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>


<%--    <script type="text/javascript">
        $(function () {
            debugger;
            SearchText();
        });
        function SearchText() {

            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TicketReport.aspx/GetAutoCompleteData",
                        data: "{'username':'" + $('#<%=txtSearch.ClientID%>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item.val == -1) {
                        return false;
                    }
                    $('#aircode').val(ui.item.val);
                }
            });
        }
    </script>--%>

    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>

    <script language="JavaScript" type="text/javascript">

        function popupLoad(ReqstID, RqustType, PNR, TktNo, PaxName, PaxType) {
            ReissueRefundPopup(ReqstID, RqustType, PNR, TktNo, PaxName, PaxType);
            if (RqustType == "Reissue") {
                $("#trCancelledBy").hide();
            }
            else {
                $("#trCancelledBy").show();
            }
            loading(); // loading
            setTimeout(function () { // then show popup, deley in .5 second
                loadPopup(); // function show popup 
            }, 100); // .5 second
            if ($('.drop option').length == 0) {
                $("#trCancelledBy").hide();
            }
        }
    </script>

    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "TicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>



    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Flight</li>
                        <li class="breadcrumb-item active" aria-current="page">Search Pax Information By PNR No</li>
                    </ol>
                </nav>
                <div class="row">



                    <div class="col-md-3">
                        <div class="form-group">
                            <%-- <label for="exampleInputPassword1">PNR</label>--%>
                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="Enter PNR" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>






                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_result" runat="server" OnClick="btn_result_Click" CssClass="btn btn-danger" Text="Search Result" />
                        </div>
                    </div>

                </div>

            </div>


            <br />
            <br />



        </div>
   
    <br />
    <br />
    <div class="card">
        <div class="card-body">
            <div class="table-responsive" id="divReport" runat="server">
                <%-- style="height:200px;overflow:scroll;"--%>
                 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvTravellerInformation" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-hover"   GridLines="None" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Pax ID" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("Title")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFname" Width="150px" runat="server" Text='<%#Eval("FName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtLname" Width="150px" runat="server" Text='<%#Eval("LName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("PaxType")%>' ReadOnly="true"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTktNoo" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTktNo" runat="server" Width="80px" Text='<%#Eval("TicketNumber")%>' ReadOnly="true" ></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="DOB">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstss" runat="server" Text='<%#Eval("DOB")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        
                                        
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                          </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
            </div>
        </div>
    </div>
    </div>
     
       
   

    <div class="loader">
    </div>
    <div id="backgroundPopup">
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function closethis() {
            $("#outerdiv").hide();
        }
    </script>
</asp:Content>

