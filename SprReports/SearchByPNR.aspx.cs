﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class SprReports_SearchByPNR : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void BindTravellerInformation()
    {
        try
        {

            // Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            SqlDataAdapter adap = new SqlDataAdapter("SearchByPNR", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@pnr", txt_PNR.Text.ToString());
            DataTable dt2 = new DataTable();
            adap.Fill(dt2);
            GvTravellerInformation.DataSource = dt2;
            GvTravellerInformation.DataBind();
        }
        catch (Exception ex)
        {
        }
    }


    protected void btn_result_Click(object sender, EventArgs e)
    {
        BindTravellerInformation();
    }
}