﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TicketReport.aspx.vb" Inherits="SprReports_TicketReport" MasterPageFile="~/MasterAfterLogin.master" Title="" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%-- <link href="<%=ResolveUrl("~/CSS/main2.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/CSS/style.css")%>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/CSS/PopupStyle.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .HeaderStyle th {
            white-space: nowrap;
        }

        .popupnew2 {
            position: absolute;
            top: 650px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .lft {
            float: left;
        }

        .rgt {
            float: right;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>

<style type="text/css">
        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            debugger;
            SearchText();
        });
        function SearchText() {

            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TicketReport.aspx/GetAutoCompleteData",
                        data: "{'username':'" + $('#<%=txtSearch.ClientID%>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item.val == -1) {
                        return false;
                    }
                    $('#aircode').val(ui.item.val);
                }
            });
        }
    </script>

    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js")%>" type="text/javascript"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js")%>"></script>

    <script language="JavaScript" type="text/javascript">

        function popupLoad(ReqstID, RqustType, PNR, TktNo, PaxName, PaxType) {
            ReissueRefundPopup(ReqstID, RqustType, PNR, TktNo, PaxName, PaxType);
            if (RqustType == "Reissue") {
                $("#trCancelledBy").hide();
            }
            else {
                $("#trCancelledBy").show();
            }
            loading(); // loading
            setTimeout(function () { // then show popup, deley in .5 second
                loadPopup(); // function show popup 
            }, 100); // .5 second
            if ($('.drop option').length == 0) {
                $("#trCancelledBy").hide();
            }
        }
    </script>

    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "TicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>



    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Flight</li>
                  <li class="breadcrumb-item active" aria-current="page">Flight Ticket Report</li>
                </ol>
              </nav>
                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<label for="exampleInputPassword1">From Date</label>--%>
                            <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<label for="exampleInputPassword1">To Date</label>--%>
                            <input type="text" name="To" id="To" placeholder="To Date" class="form-control" readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <%-- <label for="exampleInputPassword1">PNR</label>--%>
                            <asp:TextBox ID="txt_PNR" runat="server" placeholder="Enter PNR" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<label for="exampleInputPassword1">OrderId</label>--%>
                            <asp:TextBox ID="txt_OrderId" runat="server" placeholder="Order Id" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                            <asp:TextBox ID="txt_PaxName" runat="server" placeholder="PAX Name" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-3" runat="server" visible="false">
                        <div class="form-group">
                            <%-- <label for="exampleInputPassword1">Ticket No</label>--%>
                            <asp:TextBox ID="txt_TktNo" runat="server" placeholder="Ticket No." CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<label for="exampleInputPassword1">Airline</label>--%>
                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Airline" CssClass="form-control autosuggest"></asp:TextBox>
                            <input type="hidden" id="aircode" name="aircode" value="" />
                        </div>
                    </div>




                    <div class="col-md-3">
                        <div class="form-group" id="td_Agency" runat="server">
                            <%--<label for="exampleInputPassword1">Agency Name</label>--%>
                            <input type="text" id="txtAgencyName" class="form-control" name="txtAgencyName" onfocus="focusObj(this);"
                                onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group" id="tdTripNonExec2" runat="server" cssclass="form-control">
                            <%--<label for="exampleInputPassword1">Trip</label>--%>
                            <asp:DropDownList ID="ddlTripDomIntl" runat="server" CssClass="form-control">
                                <asp:ListItem Value="">-----Select Trip Type-----</asp:ListItem>
                                <asp:ListItem Value="D">Domestic</asp:ListItem>
                                <asp:ListItem Value="I">International</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btn_result" runat="server" CssClass="btn btn-danger" Text="Search Result" />                            
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <asp:Button ID="btnExpt" runat="server" CssClass="btn btn-danger" Text="Export" />
                        </div>
                    </div>





                </div>
                <div class="row" id="divPaymentMode" runat="server">

                    <div class="col-md-4">
                        <label for="exampleInputEmail1">PartnerName :</label>
                        <asp:DropDownList CssClass="form-control" ID="txtPartnerName" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="col-md-4">
                        <label for="exampleInputEmail1">PaymentMode :</label>
                        <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">
                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                            <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                        </asp:DropDownList>
                    </div>


                    <div class="col-md-4">
                        <label for="exampleInputEmail1">Search Provider </label>
                        <asp:DropDownList CssClass="form-control" ID="ddlProvider" runat="server">
                            <asp:ListItem Text="All" Selected="true" Value=""></asp:ListItem>
                            <asp:ListItem Text="TBO" Value="TB"></asp:ListItem>
                            <asp:ListItem Text="Yatra" Value="YA"></asp:ListItem>
                            <asp:ListItem Text="GAL" Value="1G"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>



                <div class="row">

                    <div class="col-md-4 d-flex">
                        <div class="d-inline-flex align-items-center justify-content-center border rounded-circle px-2 py-2 my-auto text-muted">
                            <i class="mdi mdi-server-security icon-sm my-0"></i>
                        </div>
                        <div class="wrapper pl-3">
                            <p class="mb-0 font-weight-medium text-muted">Total Ticket Sale Amount</p>

                            ₹
                            <asp:Label ID="lbl_Total" class="font-weight-semibold mb-0" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex">
                        <div class="d-inline-flex align-items-center justify-content-center border rounded-circle px-2 py-2 my-auto text-muted">
                            <i class="mdi mdi-chart-arc icon-sm my-0"></i>
                        </div>
                        <div class="wrapper pl-3">
                            <p class="mb-0 font-weight-medium text-muted">Total Ticket Issued</p>

                            <asp:Label ID="lbl_counttkt" class="font-weight-semibold mb-0 text-primary" runat="server"></asp:Label>
                        </div>
                    </div>

                </div>

                <br />
                <br />


                <div class="alert_msg info_msg fl">
                    <b class="status_info fl">Note: </b><span class="status_cont">To get Today's booking without above parameter,do not fill any field, only
                            click on Search Result.</span>

                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="card">
            <div class="card-body">
                <div class="table-responsive" id="divReport" runat="server" visible="false">
                    <%-- style="height:200px;overflow:scroll;"--%>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; ">
                        <ContentTemplate>
                            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="No records Found"  ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" CssClass="table table-hover"   GridLines="None" Width="100%"
                                PageSize="30">
                              <Columns>

                                            <asp:BoundField HeaderText="Booking Date/Time" DataField="CreateDate"></asp:BoundField>
                                            <asp:TemplateField HeaderText="OrderID">
                                                <ItemTemplate>
                                                    <a data-toggle="modal" class="txid_order" data-target="#myModal" href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe"
                                                        rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: 500; color: #004b91">
                                                        <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                  <%--<asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <%# IIf(Eval("MordifyStatus").ToString() = "Cancelled", "<p style='color:red;font-weight:bold;'>Cancelled</p>", "<p style='color:green;font-weight:bold;'>Ticketed</p>")%>
                                                                                                    </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="AgencyName">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgencyID">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgencyID" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GdsPnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AirlinePnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="AirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sector">
                                                <ItemTemplate>
                                                    <asp:Label ID="Sector" runat="server" Text='<%#Eval("Sector")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Journey Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="JourneyDate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <a href='UpdatePaxName.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                    <asp:Label ID="PaxFNAme" runat="server" Text='<%#Eval("PgTitle")%>'></asp:Label>
                                                    <asp:Label ID="PaxLName" runat="server" Text='<%#Eval("PgFName")%>'></asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("PgLName")%>'></asp:Label>
                                                                                                                </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carrier">
                                                <ItemTemplate>
                                                    <asp:Label ID="Carrier" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Trip" DataField="Trip">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Trip Type" DataField="TripType">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="BookingAmountGross" DataField="TotalBookingCost">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="BookingAmountNet" DataField="TotalAfterDis">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Status" DataField="Status">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>

                                            <asp:BoundField HeaderText="Supplier ID" DataField="TicketId">
                                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                            </asp:BoundField>
                                            <%-- <asp:TemplateField HeaderText="Payment Mode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Branch Mode">
                                                <ItemTemplate>
                                                    <asp:Label ID="Branch" runat="server" Text='<%#Eval("Branch")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Exec ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Reissue" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkreissue" runat="server" Font-Strikeout="False" Font-Overline="False"
                                                        CommandArgument='<%#Eval("PaxId") %>' CommandName='Reissue' OnClick="lnkreissue_Click"
                                                        ToolTip="Reissue Request">Reissue
                                               
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cancel" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkrefund" runat="server" Font-Strikeout="False" Font-Overline="False"
                                                        CommandArgument='<%#Eval("PaxId") %>' CommandName='Refund' OnClick="lnkrefund_Click"
                                                        ToolTip="Refund Request">Cancel
                                              
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Partner Name" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="PartnerName" runat="server" Text='<%#Eval("PartnerName")%>' Visible='<%# GetVisibleStatus() %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Convenience Fee" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPgcharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--fltHeader.GSTNO,fltHeader.GST_Company_Name,fltHeader.GST_Company_Address,fltHeader.GST_PhoneNo,fltHeader.GST_Email,fltHeader.GSTRemark--%>
                                            <asp:TemplateField HeaderText="GSTNO" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPgGSTNO" runat="server" Text='<%#Eval("GSTNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_Company_Name" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Name" runat="server" Text='<%#Eval("GST_Company_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_Company_Address" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Address" runat="server" Text='<%#Eval("GST_Company_Address")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_PhoneNo" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_PhoneNo" runat="server" Text='<%#Eval("GST_PhoneNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_Email" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Email" runat="server" Text='<%#Eval("GST_Email")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="jsgrid-header-row" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 11px; font-weight: 500; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>
    </div>


    <div style="text-decoration-line: initial; display: none" id="outerdiv" class="vew321">
        <div onclick="closethis();" title="click to close" style="background: url(../Images/closebox.png); cursor: pointer; float: right; z-index: 9999; min-height: 30px; min-width: 30px;"></div>
        <div id="ddd"></div>
    </div>
    <div id="toPopup" class="tbltbl large-12 medium-12 small-12">
        <div class="close">
        </div>
        <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table border="0" cellpadding="10" cellspacing="5" style="font-family: arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; font-style: normal; color: #000000">
                <tr>
                    <td>
                        <b>PNR :</b> <span id="PNR"></span>
                    </td>
                    <td>
                        <b>Ticket No:</b> <span id="TktNo"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>PAX NAME :</b> <span id="Paxname"></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b class="lft">Remark:</b>
                        <span>
                            <input id="RemarksType" type="text" name="RemarksType" readonly="readonly" style="border-style: none; font-weight: bold;" /></span>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea id="txtRemark" name="txtRemark" cols="40" rows="4" style="border: thin solid #808080"></textarea>
                    </td>
                </tr>
                <tr id="trCancelledBy" visible="false">
                    <td>
                        <b>Cancelled By:</b>
                    </td>
                    <td>
                        <asp:DropDownList ID="DrpCancelledBy" CssClass="drop" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div align="right">
                            <asp:Button ID="btnRemark" runat="server" Text="Submit" CssClass="button rgt" OnClientClick="return validateremark();" />
                            <input id="txtPaxid" name="txtPaxid" type="hidden" /><input id="txtPaxType" name="txtPaxType"
                                type="hidden" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>

    <div class="loader">
    </div>
    <div id="backgroundPopup">
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function closethis() {
            $("#outerdiv").hide();
        }
    </script>
	
	
	<script type="text/javascript">
        function closethis() {
            $("#outerdiv").hide();
        }
        $(".txid_order").click(function () {
            $(".txid_order").closest('tr').removeAttr("style");
            $(this).closest('tr').css("background", "#f2edf3");
            return true;
        });
    </script>
	
</asp:Content>
