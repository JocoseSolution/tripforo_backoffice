﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="UpdatePaxName.aspx.vb" Inherits="SprReports_UpdatePaxName" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 08 || charCode == 46 || charCode == 32)) {
                return true;
            }
            else {
                return false;
            }
        }

        function validateptk() {
            var ddltype = document.getElementById("typedd");
            if (ddltype.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select Status Before Processing To Hold PNR!");
                return false;
            }
        }

    </script>
     <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>

</head>


<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;
        </div>
        <div align="center">
            <table width="90%">
                <tr>
                    <td align="right" style="font-weight: bold; font-size: 15px; color: #004b91;">Booking Reference No.
                    </td>
                    <td id="tdRefNo" runat="server" align="left" style="font-weight: bold;"></td>
                </tr>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </table>


            
          
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Passenger Information
                    </td>
                </tr>
                <tr>
                    <td>
                       <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvTravellerInformation" runat="server" AutoGenerateColumns="false"
                                    Width="100%" CssClass="GridViewStyle" OnRowCancelingEdit="GvTravellerInformation_RowCancelingEdit" OnRowEditing="GvTravellerInformation_RowEditing" OnRowUpdating="GvTravellerInformation_RowUpdating">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Pax ID" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("Title")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="First Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFname" Width="150px" runat="server" Text='<%#Eval("FName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtLname" Width="150px" runat="server" Text='<%#Eval("LName")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("PaxType")%>' ReadOnly="true"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTktNoo" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTktNo" runat="server" Width="80px" Text='<%#Eval("TicketNumber")%>' ReadOnly="true" ></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Status" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstss" runat="server" Text='<%#Eval("MORDIFYSTATUS")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText ="Message" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server"  ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                         <asp:TemplateField></asp:TemplateField>
                                    <asp:CommandField HeaderText="Edit/Update" ShowDeleteButton="False" ShowEditButton="True" ShowHeader="True" />
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                          </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>

            </table>


        </div>
    </form>
</body>
</html>
