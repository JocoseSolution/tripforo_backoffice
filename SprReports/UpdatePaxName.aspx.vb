﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class SprReports_UpdatePaxName
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private ObjIntDetails As New IntlDetails()
    Dim objTktCopy As New clsTicketCopy
    Dim SelectDetails As New IntlDetails
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim TransTD As String = Request.QueryString("TransID")
                tdRefNo.InnerText = OrderId.ToString()
                'Getting Header Details
                'BindFltHeader()
                'BindLedgDetails()
                'BindMealbagg()
                'Getting Pax Details
                BindTravellerInformation()

                Dim dtagentidd As New DataTable()
                dtagentidd = ObjIntDetails.SelectAgent(Request.QueryString("OrderId"))
                ViewState("agentid") = dtagentidd.Rows(0)("AgentID").ToString()
                'BindAgency(dtagentidd.Rows(0)("AgentID").ToString())


                ''Getting Flight Details
                'BindFlightDetails()

                'Getting Fare Details
                'lblFareInformation.Text = objTktCopy.FareDetail_N(OrderId, TransTD)

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    'Public Sub BindFltHeader()
    '    Try
    '        Dim OrderId As String = Request.QueryString("OrderId")
    '        Dim dtHeader As New DataTable
    '        dtHeader = SelectHeaderDetail() 'SelectDetails.SelectHeaderDetail(OrderId)

    '        ViewState("Status_val") = dtHeader.Rows(0)("Status")
    '        ViewState("flthrds") = dtHeader
    '        GvFlightHeader.DataSource = dtHeader
    '        GvFlightHeader.DataBind()

    '        grd_GST.DataSource = dtHeader
    '        grd_GST.DataBind()

    '    Catch ex As Exception

    '    End Try
    'End Sub


    'Public Sub BindAgency(ByVal Agencyid As String)
    '    Try
    '        'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim adap As New SqlDataAdapter("AgencyINFO", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@AGENCYID", Agencyid.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)
    '        ViewState("Agency_Name") = dt2.Rows(0)("Agency_Name").ToString()
    '        ''ViewState("Crd_limit") = dt2.Rows(0)("Crd_Limit").ToString()
    '        Grd_AgencyDetails.DataSource = dt2
    '        Grd_AgencyDetails.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Public Function BindAgencyaval(ByVal Agencyid As String) As Double
    '    Dim Crd_limit As Double
    '    Try
    '        'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim adap As New SqlDataAdapter("AgencyINFO", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@AGENCYID", Agencyid.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)
    '        Crd_limit = Convert.ToDouble(dt2.Rows(0)("Crd_Limit").ToString())
    '    Catch ex As Exception

    '    End Try
    '    Return Crd_limit
    'End Function

    'Public Sub BindLedgDetails()
    '    Try
    '        Dim OrderId As String = Request.QueryString("OrderId")
    '        'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim adap As New SqlDataAdapter("ledgerInfo", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)
    '        Grd_LedgerDetails.DataSource = dt2
    '        Grd_LedgerDetails.DataBind()

    '        checkLedgr(dt2)
    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Public Sub checkLedgr(ByVal dt As DataTable)
    '    Try

    '        Dim OrderId As String = Request.QueryString("OrderId")
    '        'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim adap As New SqlDataAdapter("sp_LookUpReacord", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)

    '        Status.Visible = False
    '        MessageHold.Visible = False
    '        ddtypediv.Visible = False


    '        'Status.Visible = True
    '        'MessageHold.Visible = True
    '        'ddtypediv.Visible = True

    '        If (Convert.ToInt32(dt2.Rows(0)("pax")) > 0 And Convert.ToInt32(dt2.Rows(0)("fare")) > 0 And Convert.ToInt32(dt2.Rows(0)("flt")) > 0) Then

    '            If (ViewState("Status_val") = "Request") Then
    '                MessageHold.Visible = True
    '                ddtypediv.Visible = True
    '                Status.Visible = True
    '                If (dt.Rows.Count > 0) Then

    '                End If

    '            End If
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub


    'Public Sub BindMealbagg()
    '    Try
    '        Dim OrderId As String = Request.QueryString("OrderId")
    '        'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim adap As New SqlDataAdapter("sp_fltmealbagg", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@orderId", OrderId.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)
    '        Grd_Mealbagg.DataSource = dt2
    '        Grd_Mealbagg.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub





    Public Sub BindTravellerInformation()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_FltPaxDetails_lookup", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            GvTravellerInformation.DataSource = dt2
            GvTravellerInformation.DataBind()

        Catch ex As Exception

        End Try

    End Sub


    Public Function SelectHeaderDetail() As DataTable
        Dim dt2 As New DataTable()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_Get_FltHeaderDetailsWithPnrMessage_n", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)

            adap.Fill(dt2)


        Catch ex As Exception

        End Try
        Return dt2
    End Function


    'Public Sub BindFlightDetails()
    '    Try
    '        Dim OrderId As String = Request.QueryString("OrderId")
    '        'Dim adapFlightDetails As New SqlDataAdapter("select * from FltDetails where OrderId = '" & OrderId & "' ", con)
    '        Dim adap As New SqlDataAdapter("sp_fltdetails", con)
    '        adap.SelectCommand.CommandType = CommandType.StoredProcedure
    '        adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
    '        Dim dt2 As New DataTable()
    '        adap.Fill(dt2)
    '        GvFlightDetails.DataSource = dt2
    '        GvFlightDetails.DataBind()
    '    Catch ex As Exception

    '    End Try


    'End Sub

    'Protected Sub GvFlightHeader_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightHeader.RowCancelingEdit
    '    Try
    '        GvFlightHeader.EditIndex = -1
    '        GvFlightHeader.Columns(4).Visible = False
    '        BindFltHeader()

    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Protected Sub GvFlightHeader_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightHeader.RowEditing
    '    Try
    '        GvFlightHeader.EditIndex = e.NewEditIndex
    '        GvFlightHeader.Columns(4).Visible = True
    '        BindFltHeader()

    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Protected Sub GvFlightHeader_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightHeader.RowUpdating

    '    Try
    '        Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
    '        If txtRemark.Text = "" Then
    '            ShowAlertMessage("Please Enter Remark")
    '        Else
    '            Dim OrderId As String = Request.QueryString("OrderId")
    '            Dim txtGdsPnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtGdsPnr"), TextBox), TextBox)
    '            Dim txtAirlinePnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtAirlinePnr"), TextBox), TextBox)
    '            Dim txtStatus As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtStatus"), TextBox), TextBox)

    '            SelectDetails.UpdateFlightHeader(txtGdsPnr.Text.Trim(), txtAirlinePnr.Text.Trim(), txtStatus.Text.Trim(), OrderId)
    '            SelectDetails.InsertQCFlightHeader(OrderId, "FltHeader", Session("UID").ToString(), txtRemark.Text)

    '            lblUpdateFltHeader.Visible = True
    '            lblUpdateFltHeader.Text = "Updated Successfully"
    '            GvFlightHeader.Columns(4).Visible = False
    '            GvFlightHeader.EditIndex = -1
    '            BindFltHeader()

    '        End If

    '    Catch ex As Exception

    '    End Try


    'End Sub

    Protected Sub GvTravellerInformation_RowCancelingEdit(sender As Object, e As EventArgs)
        Try

            GvTravellerInformation.EditIndex = -1
            GvTravellerInformation.Columns(6).Visible = False
            Me.BindTravellerInformation()


        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvTravellerInformation_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvTravellerInformation.RowEditing
        Try
            GvTravellerInformation.EditIndex = e.NewEditIndex
            GvTravellerInformation.Columns(6).Visible = True
            BindTravellerInformation()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvTravellerInformation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvTravellerInformation.RowUpdating
        Try
            'Dim txtRemark As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            'If txtRemark.Text = "" Then
            '    ShowAlertMessage("Please Enter Remark")
            'Else

            Dim OrderId As String = Request.QueryString("OrderId")
                Dim lblPaxId As Label = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("lblPaxId"), Label), Label)
                Dim txtTitle As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTitle"), TextBox), TextBox)
                Dim txtFname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtFname"), TextBox), TextBox)
                Dim txtLname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtLname"), TextBox), TextBox)
                Dim txtType As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtType"), TextBox), TextBox)
                Dim txtTktNo As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTktNo"), TextBox), TextBox)

                SelectDetails.UpdatePaxInformation(txtTitle.Text.Trim(), txtFname.Text.Trim(), txtLname.Text.Trim(), txtType.Text.Trim(), txtTktNo.Text.Trim(), OrderId, lblPaxId.Text.Trim())
            'SelectDetails.InsertQCFlightHeader(OrderId, "FltPaxDetails", Session("UID").ToString(), txtRemark.Text)
            lblUpdatePax.Visible = True
                lblUpdatePax.Text = "Updated Successfully"
                GvTravellerInformation.Columns(6).Visible = False
                GvTravellerInformation.EditIndex = -1
                BindTravellerInformation()

            ' End If

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub GvFlightDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightDetails.RowEditing
    '    Try

    '        GvFlightDetails.EditIndex = e.NewEditIndex
    '        BindFlightDetails()
    '        GvFlightDetails.Columns(12).Visible = True


    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Protected Sub GvFlightDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightDetails.RowCancelingEdit
    '    Try

    '        GvFlightDetails.EditIndex = -1
    '        GvFlightDetails.Columns(12).Visible = False
    '        BindFlightDetails()

    '    Catch ex As Exception

    '    End Try


    'End Sub

    'Protected Sub GvFlightDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightDetails.RowUpdating

    '    Try
    '        Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
    '        If txtRemark.Text = "" Then
    '            ShowAlertMessage("Please Enter Remark")
    '        Else
    '            Dim OrderId As String = Request.QueryString("OrderId")
    '            Dim lblFltId As Label = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("lblFltId"), Label), Label)
    '            Dim txtDepcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityName"), TextBox), TextBox)
    '            Dim txtDepcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityCode"), TextBox), TextBox)
    '            Dim txtArrcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityName"), TextBox), TextBox)
    '            Dim txtArrcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityCode"), TextBox), TextBox)
    '            Dim txtAirlineName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineName"), TextBox), TextBox)
    '            Dim txtAirlineCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineCode"), TextBox), TextBox)
    '            Dim txtFltNo As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtFltNo"), TextBox), TextBox)
    '            Dim txtDepDate As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepDate"), TextBox), TextBox)
    '            Dim txtDepTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepTime"), TextBox), TextBox)
    '            Dim txtArrTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrTime"), TextBox), TextBox)
    '            Dim txtAirCraft As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirCraft"), TextBox), TextBox)

    '            SelectDetails.UpdateFlightDetails(txtDepcityName.Text.Trim(), txtDepcityCode.Text.Trim(), txtArrcityName.Text.Trim(), txtArrcityCode.Text.Trim(), txtAirlineName.Text.Trim(), txtAirlineCode.Text.Trim(), txtFltNo.Text.Trim(), txtDepDate.Text.Trim(), txtDepTime.Text.Trim(), txtArrTime.Text.Trim(), txtAirCraft.Text.Trim(), OrderId, lblFltId.Text)
    '            SelectDetails.InsertQCFlightHeader(OrderId, "FltDetails", Session("UID").ToString(), txtRemark.Text)

    '            lblUpdateFlight.Visible = True
    '            lblUpdateFlight.Text = "Updated Successfully"
    '            GvFlightDetails.Columns(12).Visible = False
    '            GvFlightDetails.EditIndex = -1
    '            BindFlightDetails()


    '        End If



    '    Catch ex As Exception

    '    End Try
    'End Sub

    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Function Legr() As DataTable
        Dim dt2 As DataTable = New DataTable()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim adap As SqlDataAdapter = New SqlDataAdapter("ledgerInfo", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            adap.Fill(dt2)
        Catch ex As Exception
        End Try
        Return dt2
    End Function

    'Protected Sub Status_Click(sender As Object, e As EventArgs) Handles Status.Click
    '    Dim OrderId As String = Request.QueryString("OrderId")
    '    Dim crdlmt As Double
    '    crdlmt = BindAgencyaval(ViewState("agentid").ToString())
    '    If (typedd.SelectedValue = "Confirm") Then
    '        Dim dtt As DataTable = Legr()
    '        Try
    '            BindFltHeader()
    '            If (ViewState("Status_val") = "Request") Then
    '                Status.Visible = True
    '                MessageHold.Visible = True
    '                typedd.Visible = True
    '                Dim TotalAmount_Debit As Double
    '                Dim TotalAmountD As Double
    '                TotalAmount_Debit = Convert.ToDouble(ViewState("flthrds").Rows(0)("TotalAfterDis"))

    '                If (dtt.Rows.Count) Then
    '                    TotalAmountD = Convert.ToDouble(dtt.Rows(0)("TotalDebit"))

    '                Else
    '                    TotalAmountD = 0
    '                End If
    '                If (dtt.Rows.Count > 0 And TotalAmountD >= TotalAmount_Debit) Then
    '                    '' 1 case to update fltheader Status Request to Confirm if value exists on legr table
    '                    updateConfirm()
    '                    BindFltHeader()
    '                    Status.Visible = False
    '                    MessageHold.Visible = False
    '                    typedd.Visible = False
    '                Else
    '                    '' 2nd case
    '                    Dim FltHdrDs As DataTable = ViewState("flthrds")
    '                    If (Convert.ToDouble(crdlmt) >= Convert.ToDouble(FltHdrDs.Rows(0)("TotalAfterDis"))) Then
    '                        Dim ProjectId As String = If(IsDBNull(FltHdrDs.Rows(0)("ProjectID")), Nothing, FltHdrDs.Rows(0)("ProjectID").ToString())
    '                        Dim BookedBy As String = If(IsDBNull(FltHdrDs.Rows(0)("BookedBy")), Nothing, FltHdrDs.Rows(0)("BookedBy").ToString())
    '                        Dim BillNoCorp As String = If(IsDBNull(FltHdrDs.Rows(0)("BillNoCorp")), Nothing, FltHdrDs.Rows(0)("BillNoCorp").ToString())
    '                        Dim GdsPnr As String = If(IsDBNull(FltHdrDs.Rows(0)("GdsPnr")), Nothing, FltHdrDs.Rows(0)("GdsPnr").ToString())
    '                        Dim VC As String = If(IsDBNull(FltHdrDs.Rows(0)("VC")), Nothing, FltHdrDs.Rows(0)("VC").ToString())
    '                        Dim it As Integer = Ledgerandcreditlimit_Transaction(ViewState("agentid").ToString(), Convert.ToDouble(FltHdrDs.Rows(0)("TotalAfterDis")), OrderId.Trim(), VC, GdsPnr, ViewState("Agency_Name"), Request.UserHostAddress.ToString(), ProjectId, BookedBy, BillNoCorp, crdlmt, "")
    '                        If (it > 0) Then
    '                            BindAgency(ViewState("agentid").ToString())
    '                            BindLedgDetails()
    '                            BindFltHeader()
    '                            BindMealbagg()
    '                            Status.Visible = False
    '                            MessageHold.Visible = False
    '                            typedd.Visible = False
    '                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Update Status and Inserted Successfully');", True)
    '                        End If
    '                    Else
    '                        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('insufficient Credit Limit');", True)
    '                        Status.Visible = False
    '                        MessageHold.Visible = False
    '                        typedd.Visible = False
    '                    End If
    '                End If

    '            Else
    '                Status.Visible = False
    '                MessageHold.Visible = False
    '                typedd.Visible = False
    '            End If
    '        Catch ex As Exception
    '            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Updated, Try again After Sometimes');", True)
    '            Status.Visible = False
    '            MessageHold.Visible = False
    '            typedd.Visible = False
    '        End Try

    '    Else
    '        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please -select- Status Before Processing To Hold PNR!');", True)
    '    End If
    'End Sub
    Public Sub updateConfirm()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim con_new As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            If con_new.State = ConnectionState.Closed Then
                con_new.Open()
            End If
            Dim cmd As SqlCommand = New SqlCommand("UpdateStatus", con_new)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@OrderID", OrderId.Trim)
            Dim i As Integer = cmd.ExecuteNonQuery()
            con_new.Close()
            If (i > 0) Then
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Update Status Successfully');", True)
            End If
        Catch ex As Exception
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Updated, Try again After Sometimes');", True)
        End Try
    End Sub


    'Public Function Ledgerandcreditlimit_Transaction(ByVal AGENTID As String, ByVal TOTALAFETRDIS As Double, ByVal TRACKID As String, ByVal VC As String, ByVal GDSPNR As String, ByVal AGENCYNAME As String, ByVal IP As String, ByVal ProjectId As String, ByVal BookedBy As String, ByVal BillNo As String, ByVal AvailBal As Double, ByVal EasyID As String) As Integer
    '    Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '    Dim paramHashtable As New Hashtable
    '    paramHashtable.Clear()
    '    paramHashtable.Add("@AGENTID", AGENTID)
    '    paramHashtable.Add("@TOTALAFETRDIS", TOTALAFETRDIS)
    '    paramHashtable.Add("@TRACKID", TRACKID)
    '    paramHashtable.Add("@VC", VC)
    '    paramHashtable.Add("@GDSPNR", GDSPNR)
    '    paramHashtable.Add("@AGENCYNAME", AGENCYNAME)
    '    paramHashtable.Add("@IP", IP)
    '    paramHashtable.Add("@ProjectId", ProjectId)
    '    paramHashtable.Add("@BookedBy", BookedBy)
    '    paramHashtable.Add("@BillNo", BillNo)
    '    paramHashtable.Add("@AVAILBAL", AvailBal)
    '    paramHashtable.Add("@EasyID", EasyID)
    '    paramHashtable.Add("@Status", typedd.SelectedValue)
    '    Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION_NM", 2)
    '    '' Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION_NM_DEBIT", 2)
    '    Return i
    '    'End Ledger and Credit Limit With Transaction
    'End Function
End Class

