﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="quickshow.aspx.cs" Inherits="quickshow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../../Dashboard.aspx">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Quick Show</li>
                </ol>
              </nav>

                <div class="row">
                    <div class="col-md-3">
                        <input type="text" name="From" id="From" placeholder="From Date" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="To" placeholder="To Date" id="To" class="form-control" readonly="readonly" />
                    </div>
                    <div class="col-md-3" id="tdTripNonExec2" runat="server">
                        <asp:DropDownList CssClass="form-control" ID="ddlFromCity" runat="server">
                            <asp:ListItem Value="">Select From City</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList CssClass="form-control" ID="ddlToCity" runat="server">
                            <asp:ListItem Value="">Select To City</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-3">
                        <asp:DropDownList CssClass="form-control" ID="ddlAirline" runat="server">
                            <asp:ListItem Value="">Select Airline</asp:ListItem>
                        </asp:DropDownList>
                    </div>                    
                    <div class="col-md-3">
                        <asp:TextBox ID="txtPnr" runat="server" class="form-control" placeholder="Pnr"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:LinkButton ID="btn_result" runat="server" CssClass="btn btn-danger" OnClick="btn_result_Click" OnClientClick="ClickProcess();">
                                <i class="fa fa-search" style="font-size: 10px;"></i>  Search
                        </asp:LinkButton>
                    </div>
                    <div class="col-md-3">
                        <br />
                        <asp:Label ID="lblTotalRecord" runat="server" Style="font-size: 15px;">Total Record : 0</asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <br />
       
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <asp:GridView ID="gvQuickShow" runat="server" OnPageIndexChanging="gvQuickShow_PageIndexChanging" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="table" GridLines="None" Font-Size="12px" PageSize="20" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Departure Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeparture_Date" runat="server" Text='<%# Convert.ToDateTime(Eval("Departure_Date")).ToString("dd MMM yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City From">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity_From" runat="server" Text='<%# Eval("OrgDestFrom") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City To">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity_To" runat="server" Text='<%# Eval("OrgDestTo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AirLine Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAirLineName" runat="server" Text='<%# Eval("AirLineName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pnr">
                                <ItemTemplate>
                                    <asp:Label ID="lblPnr" runat="server" Text='<%# Eval("fareBasis") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Seats">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal_Seats" runat="server" Text='<%# Eval("Total_Seats") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Used Seat">
                                <ItemTemplate>
                                    <asp:Label ID="lblUsed_Seat" runat="server" Text='<%# Eval("Used_Seat") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Available Seat">
                                <ItemTemplate>
                                    <asp:Label ID="lblAvl_Seat" runat="server" Text='<%# Eval("Avl_Seat") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script>
        function ClickProcess() {
            $(".btnprocessing").html(" Processing...<i class='fa fa-pulse fa-spinner' style='font-size: 10px;'></i>");
        }
    </script>
</asp:Content>

